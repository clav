.Dd 2019-03-27
.Dt CLAV-SDL 1
.Os
.Sh NAME
.Nm clav-sdl
.Nd Visualize quivers with SDL
.Sh SYNOPSIS
.Nm
.Oo
.Fl v
.Oc
.Oo
.Fl f
.Ar filename
.Oc
.Sh DESCRIPTION
.Nm
is a graphical program to interact with quivers.  It is similar to
Keller's Java applet, which is published at
https://webusers.imj-prg.fr/~bernhard.keller/quivermutation/ , though
.Nm
handles extensions such as half-weight edges and fat vertices.
.Sh OPTIONS
.Bl -tag -width Ds
.It Fl v
Be verbose: print a message to stdout whenever a quiver is altered.
This is the closest you'll get to an undo/redo feature.
.It Fl f
Start by opening file
.Ar filename ,
which is of the format written by
.Nm
and
.Nm clav-cli .
If this option is not provided, a rather uninteresting default quiver is loaded.
.El
.Sh INFORMATION
While
.Nm
is running, it constantly displays the current quiver. Hovering
over a vertex or an edge will display relevant information in the
top right corner.
.Pp
For convenience, edges are classified by color into three types, depending
on the specific edge weights they represent.
.Pp
.Bl -tag -width Ds
.It black
A black edge from vertex
.Ar i
to
.Ar j
has weights
.Dl ε_{ij} = ± d_{j} / GCD(d_{i}, d_{j})
.Dl ε_{ji} = ∓ d_{i} / GCD(d_{i}, d_{j})
where the signs are determined by the arrow's direction.
.It gray
A gray edge has half the edge weights that it would have if it were black.
.It red
A red edge has any other weights.  In general, red edges are rare during
.Sq interesting
mutation sequences.
.El
.Sh CONTROL
The following controls are available. To cancel an action when input is
prompted for, press
.Sq ESC .
.Bl -tag -width Ds
.It Click+Drag
Clicking and dragging a vertex will reposition it onscreen. Clicking
and dragging empty space will reposition the entire quiver.
.It m
To mutate
.Po See
.Xr clav 1
for references
.Pc at a vertex, press
.Sq m ,
then click on the vertex.
.It v
To create a new vertex, press
.Sq v ,
then click where it should be placed. You will probably want to rename
it with
.Sq r .
.It d
To delete an edge or a vertex, press
.Sq d ,
then click the edge or vertex which should be deleted.
.It e
To add an edge
.Pq create a black edge if no edge is present, or add to an edge if one is present
between two vertices, press
.Sq e ,
then click the start and end vertices.
.It h
As
.Sq e ,
but with half weight.
.It f
To increase the fatness of a vertex, press
.Sq f ,
then click the vertex.  This may change how edges of that vertex are
calculated. The maximum fatness is system-dependent but
.Sq interesting
things tend to happen with low fatness.
.It g
As
.Sq f ,
but the vertex's fatness is decreased.  The minimum fatness is 1.
.It r
To rename a vertex, press
.Sq r ,
then click the vertex to be renamed. The new name will be prompted
for. If the system has a unicode-compatible IME, entering unicode
characters should work without issue.
.It c
To change the color of a vertex, press
.Sq c ,
then click the vertex. The color will then be prompted for. The
following graphical color choosers are known and probed for
.Pq in this order :
.Bl -bullet -compact
.It
KDialog
.It
Qarma
.It
Yad
.It
Zenity
.It
Xdialog
.El
If none is found, the user may enter a color, in the form
.Ar rrggbb
where each of
.Ar r ,
.Ar b ,
.Ar g
are hexadecimal digits.
.It Shift+m/v/d/e/h/f/g/r
Holding shift while pressing one of the above buttons causes the
action to loop until explicitly terminated. For example, pressing
.Sq M ,
then clicking a sequence of 9 vertices will mutate at that sequence
more conveniently than pressing
.Sq m
9 times.
.It s
To save the current quiver, press
.Sq s .
Depending on which packages are installed on the system, the filename
will be prompted for in one of a few ways. The following graphical file
choosers are known and probed for
.Pq in this order :
.Bl -bullet -compact
.It
KDialog
.It
Qarma
.It
Yad
.It
Zenity
.It
Xdialog
.El
If none is found, the filename will be prompted for onscreen. This prompt
is rather primitive, however.
.It l
To load a quiver
.Pq and to discard the current quiver ,
press
.Sq l .
A file will be prompted for in the same manner as the save dialog.
.El
.Sh EXAMPLES
Example quivers may have been installed along with this program,
typically in
.Pa /usr/share/clav/ .
.Sh SEE ALSO
.Xr clav 1
.Xr clav-cli 1
.Sh BUGS
Edge weights are, for performance reasons
.Po see
.Xr clav-mutation-find 1
.Pc , stored as fractions with
numerator and denominator in system-dependent ranges.  While it probably
will not be an issue for
.Sq interesting
usage, causing edge weights to increase arbitrarily will eventually cause
.Nm
to compute an unrepresentable edge weight
.Pq for example, 1/257 might be unrepresentable .
In this situation,
.Nm
will not perform the mutation, rather than display incorrect
results.
.Sh AUTHORS
.An S. Gilles Aq Mt sgilles@math.umd.edu
