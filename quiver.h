/*
 * Copyright (c) 2016-2019, S. Gilles <sgilles@math.umd.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/* A rational number */
struct rational {
        /* Numerator */
        int_fast32_t p;

        /* Denominator */
        uint_fast32_t q;
};

/* The structure for a vertex */
struct vertex {
        /* A textual identifier for this vertex */
        char *name;

        /* Whether this vertex is `fat' or not. Expected to be 1, 2, or 3. */
        uint_fast8_t fatness;

        /* Drawing data: coordinates in `internal coordinates */
        int x;
        int y;

        /* Drawing data: color */
        uint_fast8_t r;
        uint_fast8_t g;
        uint_fast8_t b;
};

/* The cluster algebra structure */
struct quiver {
        /* How many vertices there actually are in the quiver */
        size_t v_num;

        /* How many vertices we have allocated memory for */
        size_t v_len;

        /* Vertices */
        struct vertex *v;

        /* Edges - e_{ij} (fatness included, so E^t is not necessarily -E) */
        struct rational *e;

        /* For detecting if quiver_mutate() creates an edge too high */
        unsigned int edge_weight_threshold;
};

/* Add an arrow of weight a/b from i -> j, affecting e(i,j) and e(j,i) */
int quiver_add_to_edge(struct quiver *q, size_t i, size_t j, int_fast32_t a,
                       uint_fast32_t b, const char **out_errstr);

/* Add a vertex with a name and weight */
int quiver_add_vertex(struct quiver *q, size_t *out_i, const char *name,
                      uint_fast16_t fatness, int x, int y, uint32_t argb, const
                      char **out_errstr);

/* Increase or decrease the fatness of a vertex, readjusting edges as well */
int quiver_adjust_fatness(struct quiver *q, size_t i, int_fast8_t fatness, const
                          char **out_errstr);

/* Free all memory used by this quiver, resetting it to { 0 } */
int quiver_clean(struct quiver *q);

/* Delete a vertex (and all associated edges) */
int quiver_delete_vertex(struct quiver *q, size_t i, const char **out_errstr);

/* Return σ_{ij} */
int quiver_get_sigma(const struct quiver * const q, size_t i, size_t j, struct
                     rational *out_s);

/* Read quiver from file */
int quiver_load_from_file(struct quiver *q, FILE *f, const char **out_errstr);

/* Rename a vertex */
int quiver_rename_vertex(struct quiver *q, size_t i, const char *new_name, const
                         char **out_errstr);

/* Recolor a vertex */
int quiver_color_vertex(struct quiver *q, size_t i, uint32_t argb, const
                        char **out_errstr);

/* Serialize the quiver */
int quiver_save_to_file(struct quiver *q, FILE *f, const char **out_errstr);

/* Set threshold: if quiver_mutate() creates an edge above, warn_out is set */
int quiver_set_warning_threshold(struct quiver *q, unsigned int warn_threshold,
                                 const char **out_errstr);

/* Mutate the quiver at vertex k, setting warn_out if threshold is broken */
int quiver_mutate(struct quiver *q, size_t k, int *out_warn, const
                  char **out_errstr);
