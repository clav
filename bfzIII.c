/*
 * Copyright (c) 2016-2021, S. Gilles <sgilles@sgilles.net>
 *
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <errno.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "macros.h"
#include "quiver.h"

/*
 * This is an implementation of the algorithm of Berenstein, Fomin,
 * and Zelevinsky from "Cluster Algebras III", which produces quivers
 * for the double Bruhat cell G^{u,v}.
 */

#define zabs(x)    (((x) < 0) ? -(x) : (x))
#define zabsdiff(x, y) ((x) < (y) ? ((y) - (x)) : ((x) - (y)))
#define zmin(x, y) (((x) < (y)) ? (x) : (y))
#define zmax(x, y) (((x) > (y)) ? (x) : (y))
#define zsgn(x) ((x) < 0 ? -1 : (x) > 0 ? 1 : 0)

/* Which of the Lie classifications we're looking at */
enum dynkin { INVALID, An, Bn, Cn, Dn, G2, F4, E6, E7, E8 };

/* Cartan matrix entry a_{i,j}, given dynkin type. 1-based. */
static int
aij(enum dynkin typ, size_t n, size_t i, size_t j)
{
        i--;
        j--;
        size_t max_idx = (i > j) ? i : j;
        size_t k = 0;

        switch (typ) {
        case INVALID:

                return INT_MAX;
                break;
        case An:

                if (max_idx >= n) {
                        return INT_MAX;
                } else if (i == j) {
                        return 2;
                } else if (zabsdiff(i, j) == 1) {
                        return -1;
                }

                return 0;
                break;
        case Bn:
        case Cn:

                /* TODO: do you have Bn and Cn switched? You always do that */
                if (max_idx >= n) {
                        return INT_MAX;
                } else if (i == j) {
                        return 2;
                } else if (zabsdiff(i, j) == 1) {
                        if (typ == Cn &&
                            i == n - 2 &&
                            j == n - 1) {
                                return -2;
                        } else if (typ == Bn &&
                                   i == n - 1 &&
                                   j == n - 2) {
                                return -2;
                        }

                        return -1;
                }

                return 0;
                break;
        case Dn:

                if (max_idx >= n) {
                        return INT_MAX;
                } else if (i == j) {
                        return 2;
                } else if (zabsdiff(i, j) == 1 &&
                           i <= n - 2 &&
                           j <= n - 2) {
                        return -1;
                } else if ((i == n - 3 &&
                            j == n - 1) ||
                           (i == n - 1 &&
                            j == n - 3)) {
                        return -1;
                }

                return 0;
                break;
        case G2:

                if (max_idx >= 2) {
                        return INT_MAX;
                } else if (i == j) {
                        return 2;
                } else if (i == 0 &&
                           j == 1) {
                        return -3;
                } else if (i == 1 &&
                           j == 0) {
                        return -1;
                }

                return 0;
                break;
        case F4:

                if (max_idx >= 4) {
                        return INT_MAX;
                } else if (i == j) {
                        return 2;
                } else if (i == 1 &&
                           j == 2) {
                        return -2;
                } else if (zabsdiff(i, j) == 1) {
                        return -1;
                }

                return 0;
                break;
        case E6:
        case E7:
        case E8:
                k = (typ == E6) ? 6 : (typ == E7) ? 7 : 8;

                if (max_idx >= k) {
                        return INT_MAX;
                } else if (i == j) {
                        return 2;
                } else if (zabsdiff(i, j) == 1 &&
                           zmax(i, j) >= 3) {
                        return -1;
                } else if (zabsdiff(i, j) == 2 &&
                           zmin(i, j) <= 1) {
                        return -1;
                }

                return 0;
                break;
        }

        return INT_MAX;
}

const int x_mult = 25;
const int y_mult = 25;

/* Given vertex idx of k, return vertex idx of k⁺ */
static int
find_plus(int k, size_t *u, size_t lu, size_t *v, size_t lv)
{
        size_t vk = (size_t) -1;

        if (k < 0) {
                vk = -1 * (k + 1);

                for (int j = 1; j <= (int) (lu + lv); ++j) {
                        size_t vj = (size_t) -1;

                        vj = (j <= (int) lu) ? u[j - 1] : v[j - 1 - lu];

                        if (vk == vj) {
                                return j;
                        }
                }
        } else if (1 <= k &&
                   k <= (int) lu) {
                vk = u[k - 1];

                for (int j = k + 1; j <= (int) (lu + lv); ++j) {
                        size_t vj = (size_t) -1;

                        vj = (j <= (int) lu) ? u[j - 1] : v[j - 1 - lu];

                        if (vk == vj) {
                                return j;
                        }
                }
        } else if ((int) lu + 1 <= k &&
                   k <= (int) (lu + lv)) {
                vk = v[k - 1 - lu];

                for (int j = k + 1; j <= (int) (lu + lv); ++j) {
                        size_t vj = v[j - 1 - lu];

                        if (vk == vj) {
                                return j;
                        }
                }
        }

        return lu + lv + 1;
}

/* Given k in { -1, ..., -r } U { 1, 2, ..., l(u) + l(v) }, return the vertex idx for it */
static size_t
v_of(int k, int r, size_t lu, size_t lv)
{
        if (-1 >= k &&
            k >= -r) {
                return (-1 * k) - 1;
        } else if (1 <= k &&
                   k <= (int) (lu + lv)) {
                return k - 1 + r;
        }

        return (size_t) -1;
}

/* [BFZIII] */
static int
run_bfzIII(struct quiver *qq, enum dynkin typ, size_t r, size_t *u, size_t lu,
           size_t *v, size_t lv, int ymult)
{
        int ret = -1;
        size_t nl = 1 + snprintf(0, 0, "%d%zu", -1 * (int) r, lu + lv);
        char *nbuf = 0;
        const char *errstr = 0;
        size_t x = 0;
        int *i = 0;
        int *e = 0;
        uint32_t c_froz = 0x4ce7ff;
        uint32_t c_nonf = 0xad7fa8;
        int need_fatness_run = 1;

        if (!(nbuf = malloc(nl))) {
                goto done;
        }

        if (!(i = calloc(r + lu + lv + 1, sizeof *i))) {
                goto done;
        }

        if (!(e = calloc(r + lu + lv + 1, sizeof *e))) {
                goto done;
        }

        /* First, add { -1, -2, ..., -r } */
        for (int k = -1; k >= -1 * (int) r; k--) {
                size_t vv = v_of(k, r, lu, lv);

                i[vv] = -1 * k;
                sprintf(nbuf, "%d", k);

                if (quiver_add_vertex(qq, 0, nbuf, 1, 50 * x, ymult * 50 *
                                      i[vv], c_froz, &errstr) < 0) {
                        fprintf(stderr, "%s\n", errstr);
                        fprintf(stderr, L("quiver_add_vertex failed\n"));
                        goto done;
                }

                x++;
        }

        /* Now add { 1, 2, ..., u_seq_len + v_seq_len } */
        for (int k = 1; k <= (int) (lu + lv); ++k) {
                size_t vv = v_of(k, r, lu, lv);
                int kplus = find_plus(k, u, lu, v, lv);
                uint32_t c = (kplus <= (int) (lu + lv)) ? c_nonf : c_froz;

                i[vv] = 1 + ((k <= (int) lu) ? u[k - 1] : v[k - lu - 1]);
                sprintf(nbuf, "%d", k);

                if (quiver_add_vertex(qq, 0, nbuf, 1, 50 * x, ymult * 50 *
                                      i[vv], c, &errstr) < 0) {
                        fprintf(stderr, "%s\n", errstr);
                        fprintf(stderr, L("quiver_add_vertex failed\n"));
                        goto done;
                }

                x++;
        }

        /* Now add edges by remark 2.4 */
        for (int k = -r; k <= (int) (lu + lv); ++k) {
                int kplus = find_plus(k, u, lu, v, lv);
                size_t vk = v_of(k, r, lu, lv);

                if (k == 0) {
                        continue;
                }

                for (int l = -r; l <= (int) (lu + lv); ++l) {
                        int lplus = find_plus(l, u, lu, v, lv);
                        int p = zmax(k, l);
                        int eip = (1 <= p &&
                                   p <= (int) lu) ? -1 : 1;
                        int q = zmin((int) kplus, (int) lplus);
                        int eiq = (1 <= q &&
                                   q <= (int) lu) ? -1 : 1;
                        size_t vl = v_of(l, r, lu, lv);
                        size_t idx = vk * qq->v_len + vl;
                        struct rational *e = &qq->e[idx];

                        if (l == 0 ||
                            (k < 0 &&
                             l < 0)) {
                                continue;
                        }

                        if (p == q) {
                                e->p = zsgn(k - l) * eip;
                                e->q = 1;
                        } else if (p < q &&
                                   eip * eiq * (k - l) * (kplus - lplus) > 0) {
                                e->p = zsgn(k - l) * eip * aij(typ, r, i[vk],
                                                               i[vl]);
                                e->q = 1;
                        }
                }
        }

        /*
         * Now modify the fatness of various vertices to normalize |sigma|.
         *
         * We could just compute this by looking at the y-position
         * and the Dynkin type, but this serves as a nice sanity check.
         */
        while (need_fatness_run) {
                need_fatness_run = 0;

                for (size_t j = 0; j < qq->v_num; ++j) {
                        struct vertex *v = &qq->v[j];

                        for (size_t k = 0; k < qq->v_num; ++k) {
                                struct vertex *w = &qq->v[k];
                                struct rational *e = &qq->e[j * qq->v_len + k];
                                struct rational *f = &qq->e[k * qq->v_len + j];

                                if (zabs(e->p * v->fatness) > zabs(f->p *
                                                                   w->fatness))
                                {
                                        w->fatness++;
                                        need_fatness_run = 1;
                                } else if (zabs(e->p * v->fatness) < zabs(f->p *
                                                                          w->
                                                                          fatness))
                                {
                                        v->fatness++;
                                        need_fatness_run = 1;
                                }
                        }
                }
        }

        quiver_save_to_file(qq, stdout, 0);
        ret = 0;
done:
        free(nbuf);
        free(i);
        free(e);

        return ret;
}

/* "e" -> { }, "1,2" -> { 1, 2 } */
static int
parse_word(const char *s, size_t n, size_t **out_word, size_t *out_len)
{
        size_t *seq = 0;
        size_t len = 1;
        size_t j = 0;
        int ret = -1;
        char *err = 0;

        if (!(strcmp(s, "e"))) {
                *out_word = 0;
                *out_len = 0;

                return 0;
        }

        for (const char *p = s; *p; p++) {
                len += (*p == ',');
        }

        if (!(seq = calloc(len, sizeof *seq))) {
                perror(L("calloc"));
                goto done;
        }

        errno = 0;
        seq[j] = strtoll(s, 0, 0) - 1;

        if (seq[j] >= n ||
            errno) {
                goto done;
        }

        j++;

        for (const char *p = s; *p; p++) {
                if (*p != ',') {
                        continue;
                }

                errno = 0;
                seq[j] = strtoll(p + 1, &err, 0) - 1;

                if (seq[j] >= n ||
                    errno) {
                        goto done;
                }

                j++;
        }

        ret = 0;
done:

        if (ret < 0) {
                free(seq);
        } else {
                *out_word = seq;
                *out_len = len;
        }

        return ret;
}

static void
usage(char *argv0)
{
        printf(
                "Usage: %s -c <Dynkin classification> [ -u <word>]  [-v <word>] [ -U ]\n",
                argv0);
        printf(
                "       Dynkin classification: Something like \u2018F4\u2019; permitted are\n");
        printf("           A\u2081, A\u2082, A\u2083, \u2026\n");
        printf("           B\u2081, B\u2082, B\u2083, \u2026\n");
        printf("           C\u2081, C\u2082, C\u2083, \u2026\n");
        printf("           D\u2081, D\u2082, D\u2083, \u2026\n");
        printf("           G\u2082, F\u2084, E\u2086, E\u2087, E\u2088\n");
        printf(
                "       Word: something like \u20181,2,1,2\u2019 (\u2018e\u2019 is permitted)\n");
        printf("             if none given, \u2018e\u2019 is assumed\n");
        printf("\n");
        printf("        With -U, -r is above -1. Without, -1 is above -r\n");
}

/* Run the thing */
int
main(int argc, char **argv)
{
        int ret = 0;
        int opt = 0;
        enum dynkin typ = INVALID;
        int n = -1;
        char *p = 0;
        char *uword = "e";
        char *vword = "e";
        int ymult = 1;
        size_t *u_seq = 0;
        size_t *v_seq = 0;
        size_t u_seq_len = 0;
        size_t v_seq_len = 0;
        struct quiver q = { 0 };

        while ((opt = getopt(argc, argv, "hUc:u:v:")) != -1) {
                switch (opt) {
                case 'h':
                        usage(argv[0]);
                        ret = 0;
                        goto done;
                case 'u':
                        uword = optarg;
                        break;
                case 'v':
                        vword = optarg;
                        break;
                case 'U':
                        ymult = -1;
                        break;
                case 'c':

                        switch (optarg[0]) {
                        case 'a':
                        case 'A':
                                typ = An;
                                n = strtoll(optarg + 1, &p, 0);
                                break;
                        case 'b':
                        case 'B':
                                typ = Bn;
                                n = strtoll(optarg + 1, &p, 0);
                                break;
                        case 'c':
                        case 'C':
                                typ = Cn;
                                n = strtoll(optarg + 1, &p, 0);
                                break;
                        case 'd':
                        case 'D':
                                typ = Dn;
                                n = strtoll(optarg + 1, &p, 0);
                                break;
                        case 'e':
                        case 'E':
                                n = strtoll(optarg + 1, &p, 0);

                                switch (n) {
                                case 6:
                                        typ = E6;
                                        break;
                                case 7:
                                        typ = E7;
                                        break;
                                case 8:
                                        typ = E8;
                                        break;
                                default:
                                        fprintf(stderr,
                                                "Type E may only be E\u2086, E\u2087, or E\u2088\n");
                                        n = -1;
                                        break;
                                }

                                break;
                        case 'f':
                        case 'F':

                                if (optarg[1] == '4' &&
                                    optarg[2] == 0) {
                                        typ = F4;
                                        n = 4;
                                } else {
                                        fprintf(stderr,
                                                "Type F may only be F\u2084\n");
                                }

                                break;
                        case 'g':
                        case 'G':

                                if (optarg[1] == '2' &&
                                    optarg[2] == 0) {
                                        typ = G2;
                                        n = 2;
                                } else {
                                        fprintf(stderr,
                                                "Type G may only be G\u2082\n");
                                }

                                break;
                        default:
                                fprintf(stderr,
                                        "Invalid Dynkin classification: \u2018%s\u2019\n",
                                        optarg);
                                goto done;
                                break;
                        }

                        if (n <= 0 ||
                            (p &&
                             *p)) {
                                fprintf(stderr, "Invalid n: \u2018%s\u2019\n",
                                        optarg + 1);
                                ret = EINVAL;
                                goto done;
                        }
                }
        }

        switch (typ) {
        case INVALID:
                fprintf(stderr,
                        "Dynkin classification is required (ex: \u2018-c F4\u2019)\n");
                ret = EINVAL;
                goto done;
                break;
        default:
                break;
        }

        if (parse_word(uword, n, &u_seq, &u_seq_len) < 0) {
                fprintf(stderr, "Invalid u-word: \u2018%s\u2019\n", uword);
                ret = EINVAL;
                goto done;
        }

        if (parse_word(vword, n, &v_seq, &v_seq_len) < 0) {
                fprintf(stderr, "Invalid v-word: \u2018%s\u2019\n", vword);
                ret = EINVAL;
                goto done;
        }

        if ((ret = run_bfzIII(&q, typ, n, u_seq, u_seq_len, v_seq, v_seq_len,
                              ymult)) < 0) {
                fprintf(stderr, "BFZIII algorithm failed\n");
                goto done;
        }

done:
        quiver_clean(&q);
        free(u_seq);
        free(v_seq);

        return ret;
}
