/*
 * Copyright (c) 2016-2021, S. Gilles <sgilles@sgilles.net>
 *
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <errno.h>
#include <locale.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h>

#include "macros.h"
#include "quiver.h"
#include "ui.h"

/* Quit yet? */
static uint_fast8_t should_quit = 0;

/* if (verbose) printf(...); */
static void
history(int verbose, const char *format, ...)
{
        va_list arglist;

        if (!verbose) {
                return;
        }

        printf("action: ");
        va_start(arglist, format);
        vprintf(format, arglist);
        va_end(arglist);
        printf("\n");
}

/* Having received some kind of input, change state accordingly */
static int
state_transition(struct ui_event *e, struct quiver *q, int verbose)
{
        int ret = 0;
        FILE *f = 0;
        const char *errstr = 0;

        switch (e->type) {
        case ET_QUIT:
                should_quit = 1;
                history(verbose, "quit");
                break;
        case ET_RENAME:

                if (!e->str ||
                    !e->str[0] ||
                    e->idx_1 >= q->v_num) {
                        break;
                }

                history(verbose, "rename \"%s\" to \"%s\"", q->v[e->idx_1].name,
                        e->str);

                if ((ret = quiver_rename_vertex(q, e->idx_1, e->str,
                                                &errstr))) {
                        fprintf(stderr, "%s\n", UBSAFES(errstr));
                }

                ui_respond_quiver_change();
                break;
        case ET_SAVE:

                if (!e->str ||
                    !e->str[0]) {
                        break;
                }

                history(verbose, "save to \"%s\"", e->str);

                if (!(f = fopen(e->str, "w"))) {
                        ret = errno;
                        perror(L("fopen"));
                } else if ((ret = quiver_save_to_file(q, f, &errstr))) {
                        fprintf(stderr, "%s\n", UBSAFES(errstr));
                } else {
                        ui_respond_successful_save(e->str);
                }

                break;
        case ET_LOAD:

                if (!e->str ||
                    !e->str[0]) {
                        break;
                }

                history(verbose, "load from \"%s\"", e->str);

                if (!(f = fopen(e->str, "r"))) {
                        ret = errno;
                        perror(L("fopen"));
                } else if ((ret = quiver_load_from_file(q, f, &errstr))) {
                        fprintf(stderr, "%s\n", UBSAFES(errstr));
                } else {
                        ui_respond_successful_load(e->str);
                }

                ui_respond_quiver_change();
                break;
        case ET_MUTATE:

                if (e->idx_1 < q->v_num) {
                        history(verbose, "mutate at \"%s\" (%zu)",
                                q->v[e->idx_1].name, e->idx_1);

                        if ((ret = quiver_mutate(q, e->idx_1, 0, &errstr))) {
                                fprintf(stderr, "%s\n", UBSAFES(errstr));
                        }
                }

                break;
        case ET_NEW_VERTEX:

                if (!e->str ||
                    !e->str[0]) {
                        break;
                }

                history(verbose, "new vertex \"%s\" at (%d, %d)", e->str,
                        e->int_1, e->int_2);

                if ((ret = quiver_add_vertex(q, 0, e->str, 1, e->int_1,
                                             e->int_2, e->z, &errstr))) {
                        fprintf(stderr, "%s\n", UBSAFES(errstr));
                }

                ui_respond_quiver_change();
                break;
        case ET_NEW_EDGE:

                if (e->idx_1 < q->v_num &&
                    e->idx_2 < q->v_num) {
                        history(verbose,
                                "add edge from \"%s\" (%zu) to \"%s\" (%zu), weight %d/%d",
                                q->v[e->idx_1].name, e->idx_1,
                                q->v[e->idx_2].name,
                                e->idx_2, (int) e->a, (int) e->b);

                        if ((ret = quiver_add_to_edge(q, e->idx_1, e->idx_2,
                                                      e->a, e->b, &errstr))) {
                                fprintf(stderr, "%s\n", UBSAFES(errstr));
                        }
                }

                break;
        case ET_DELETE_VERTEX:

                if (e->idx_1 < q->v_num) {
                        history(verbose, "delete vertex \"%s\" (%zu)",
                                q->v[e->idx_1].name, e->idx_1);

                        if ((ret = quiver_delete_vertex(q, e->idx_1,
                                                        &errstr))) {
                                fprintf(stderr, "%s\n", UBSAFES(errstr));
                        }
                }

                ui_respond_quiver_change();
                break;
        case ET_DELETE_EDGE:

                if (e->idx_1 < q->v_num &&
                    e->idx_2 < q->v_num) {
                        size_t i12 = e->idx_1 * q->v_len + e->idx_2;
                        size_t i21 = e->idx_2 * q->v_len + e->idx_1;

                        history(verbose,
                                "remove edge between \"%s\" (%zu) and \"%s\" (%zu)",
                                q->v[e->idx_1].name, e->idx_1,
                                q->v[e->idx_2].name,
                                e->idx_2);
                        q->e[i12] = (struct rational) { .p = 0, .q = 1 };
                        q->e[i21] = (struct rational) { .p = 0, .q = 1 };
                }

                break;
        case ET_CHANGE_FATNESS:

                if (e->idx_1 < q->v_num &&
                    q->v[e->idx_1].fatness + e->int_1 > 0 &&
                    q->v[e->idx_1].fatness + e->int_1 < UINT_FAST8_MAX) {
                        history(verbose,
                                "change fatness of vertex \"%s\" (%zu) to %d",
                                q->v[e->idx_1].name, e->idx_1,
                                q->v[e->idx_1].fatness +
                                e->int_1);

                        if ((ret = quiver_adjust_fatness(q, e->idx_1, e->int_1,
                                                         &errstr))) {
                                fprintf(stderr, "%s\n", UBSAFES(errstr));
                        }
                }

                break;
        case ET_CHANGE_COLOR:

                if (e->idx_1 < q->v_num) {
                        history(verbose,
                                "change color of vertex \"%s\" (%zu) to %#010lx",
                                q->v[e->idx_1].name, e->idx_1, (long int) e->z);

                        if ((ret = quiver_color_vertex(q, e->idx_1, e->z,
                                                       &errstr))) {
                                fprintf(stderr, "%s\n", UBSAFES(errstr));
                        }
                }

                break;
        case ET_NONE:
                break;
        }

        if (f) {
                fclose(f);
        }

        return ret;
}

/* Main loop */
int
main(int argc, char **argv)
{
        int ret = 1;
        struct quiver q = { 0 };
        int opt;
        int verbose = 0;
        const char *initial_file = 0;

        UNUSED(argc);
        UNUSED(argv);
        setlocale(LC_ALL, "");

        while ((opt = getopt(argc, argv, "vf:")) != -1) {
                switch (opt) {
                case 'v':
                        verbose = 1;
                        break;
                case 'f':
                        initial_file = optarg;
                        break;
                }
        }

        if ((ret = ui_init(&q))) {
                goto done;
        }

        /* Preload a basic quiver */
        if (initial_file) {
                FILE *f = 0;

                if ((f = fopen(initial_file, "r"))) {
                        if (!(quiver_load_from_file(&q, f, 0))) {
                                if ((ret = ui_respond_successful_load(
                                             initial_file))) {
                                        goto done;
                                }
                        }

                        fclose(f);
                }
        } else {
                size_t va, vb, vc;

                quiver_add_vertex(&q, &va, "A", 1, -50, 50, 0x008282b2, 0);
                quiver_add_vertex(&q, &vb, "B", 1, 0, -75, 0x008282b2, 0);
                quiver_add_vertex(&q, &vc, "C", 1, 75, 0, 0x008282b2, 0);
                quiver_add_to_edge(&q, va, vb, 1, 1, 0);
                quiver_add_to_edge(&q, vc, va, 1, 1, 0);
        }

        if ((ret = ui_respond_quiver_change())) {
                goto done;
        }

        if ((ret = ui_start_frame())) {
                goto done;
        }

        uint_fast8_t more_evs = 0;
        struct ui_event e = { 0 };

        while (!should_quit) {
                /* includes framelimiting delay */
                if ((ret = ui_finish_frame())) {
                        goto done;
                }

                more_evs = 1;

                while (more_evs) {
                        if ((ret = ui_get_event(&e, &more_evs))) {
                                goto done;
                        }

                        if (e.type) {
                                state_transition(&e, &q, verbose);
                        }
                }

                if ((ret = ui_start_frame())) {
                        goto done;
                }
        }

done:
        ui_teardown();
        quiver_clean(&q);

        return ret;
}
