/*
 * Copyright (c) 2016-2021, S. Gilles <sgilles@sgilles.net>
 *
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <SDL.h>
#include <SDL_ttf.h>

#include "color-selection.h"
#include "file-selection.h"
#include "macros.h"
#include "quiver.h"
#include "ui-sdl-font.h"
#include "ui.h"

#define TICKS_PER_FRAME (1000 / 60)

#ifndef M_PI
#define M_PI (3.14159265358979323846)
#endif

/* What clicking does */
static enum ui_action {
        UIA_NONE = 0,
        UIA_ASK_QUIT,
        UIA_DEC_FATNESS,
        UIA_DELETE,
        UIA_ENTER_LOAD,
        UIA_ENTER_RENAME,
        UIA_ENTER_SAVE,
        UIA_ENTER_COLOR,
        UIA_INC_FATNESS,
        UIA_CHANGE_COLOR,
        UIA_MUTATE,
        UIA_NEW_EDGE_1,
        UIA_NEW_EDGE_2,
        UIA_NEW_H_EDGE_1,
        UIA_NEW_H_EDGE_2,
        UIA_NEW_VERTEX,
        UIA_RENAME,
        UIA_LEN
} ui_action;

/* Whether to cycle the state indefinitely (add many vertices, &c) */
static uint_fast8_t ui_action_loop;

/* The window we'll be using */
static SDL_Window *sdl_win;

/* The renderer we'll be using */
static SDL_Renderer *sdl_renderer;

/* How to limit the framerate */
static Uint32 frame_start_ticks;

/* Whether the scene needs to be drawn again */
static uint_fast8_t redraw;

/* Whether a vertex was just dragged (necessitating info redraw) */
static uint_fast8_t dragged_selected_vertex;

/* The informative texture */
static SDL_Texture *selected_info;

/* Buffer for event queue */
static struct ui_event *eq_buf;

/* Current max length of event queue */
static size_t eq_len;

/* Current head of queue */
static size_t eq_head;

/* Current tail of queue */
static size_t eq_tail;

/* The quiver we'll be using */
static struct quiver *q;

/* Width of drawing area in pixels */
static int da_pix_width;

/* Height of drawing area in pixels */
static int da_pix_height;

/* The background color */
static SDL_Color color_bg = { .r = 0xe2, .g = 0xe2, .b = 0xe2, .a = 0xff };

/* The normal vertex color */
static SDL_Color color_v = { .r = 0x82, .g = 0x82, .b = 0xb2, .a = 0xff };

/* The vertex color for preview vertices */
static SDL_Color color_v_preview = { .r = 0x82, .g = 0x82, .b = 0xb2, .a =
                                             0x40 };

/* The normal vertex outline color */
static SDL_Color color_outline = { .r = 0x12, .g = 0x12, .b = 0x12, .a = 0x80 };

/* The vertex outline color for preview vertices */
static SDL_Color color_outline_preview = { .r = 0x12, .g = 0x12, .b = 0x12, .a =
                                                   0x20 };

/* The selected vertex outline  color */
static SDL_Color color_outline_sel = { .r = 0x42, .g = 0x42, .b = 0xe2, .a =
                                               0x80 };

/* The font color */
static SDL_Color color_font = { .r = 0x12, .g = 0x12, .b = 0x12, .a = 0x80 };

/* The normal edge color */
static SDL_Color color_e_normal = { .r = 0x12, .g = 0x12, .b = 0x12, .a =
                                            0xff };

/* The half edge color */
static SDL_Color color_e_half = { .r = 0x12, .g = 0x12, .b = 0x12, .a = 0x60 };

/* The abnormal edge color */
static SDL_Color color_e_abnormal = { .r = 0xd0, .g = 0x12, .b = 0x12, .a =
                                              0xf0 };

/* The selected edge color */
static SDL_Color color_e_sel = { .r = 0xb2, .g = 0xb2, .b = 0xe2, .a = 0x40 };

/* The font for node names, instructions, etc */
static TTF_Font *normal_font;

/* SDL wants a SDL_RWops pointer to deal with in-memory font */
static SDL_RWops *normal_font_rw;

/* The base font size */
static uint_fast8_t base_font_size = 12;

/* How much space (pixels) between lower left of screen and bottom text */
static unsigned int text_border_padding = 24;

/* Strings to display at bottom of screen */
static const char *bottom_string[] = {
        /* */
        [UIA_NONE] = "[m] Mutate\n[v] Create new vertex\n"
                     "[d] Delete vertex/edge\n[e] Add edge\n"
                     "[h] Add half edge\n[f] Increase vertex fatness\n"
                     "[g] Decrease vertex fatness\n[r] Rename vertex\n"
                     "[c] Change vertex color\n[Shift+key] as [key] "
                     "above, but loop\n[s] Save quiver\n[l] "
                     "Load quiver\n[q] Quit",                           /* */
        [UIA_ASK_QUIT] = "Quit?\n\n[y] Confirm\n[n] Cancel",            /* */
        [UIA_DEC_FATNESS] = "[Mouse1] Decrease fatness\n[ESC] Cancel",  /* */
        [UIA_DELETE] = "[Mouse1] Delete vertex/edge\n[ESC] Cancel",     /* */
        [UIA_ENTER_LOAD] = "[Enter] Load from\n[ESC] Cancel",           /* */
        [UIA_ENTER_RENAME] = "[Enter] Rename\n[ESC] Cancel",            /* */
        [UIA_ENTER_SAVE] = "[Enter] Save to\n[ESC] Cancel",             /* */
        [UIA_ENTER_COLOR] = "[Enter] Use color (rrggbb)\n[ESC] Cancel", /* */
        [UIA_INC_FATNESS] = "[Mouse1] Increase fatness\n[ESC] Cancel",  /* */
        [UIA_CHANGE_COLOR] = "[Mouse1] Change color\n[ESC] Cancel",     /* */
        [UIA_MUTATE] = "[Mouse1] Mutate at vertex\n[ESC] Cancel",       /* */
        [UIA_NEW_EDGE_1] = "[Mouse1] Select start\n[ESC] Cancel",       /* */
        [UIA_NEW_EDGE_2] = "[Mouse1] Select end\n[ESC] Cancel",         /* */
        [UIA_NEW_H_EDGE_1] = "[Mouse1] Select start\n[ESC] Cancel",     /* */
        [UIA_NEW_H_EDGE_2] = "[Mouse1] Select end\n[ESC] Cancel",       /* */
        [UIA_NEW_VERTEX] = "[Mouse1] Create new vertex\n[ESC] Cancel",  /* */
        [UIA_RENAME] = "[Mouse1] Rename vertex\n[ESC] Cancel",          /* */
        [UIA_LEN] = ""                                                  /* */
};

/* The texture containing what to show on the bottom */
static SDL_Texture *bottom_text[UIA_LEN];

/* The textures containing the names of all vertices - indexed just like q */
static SDL_Texture **vertex_names;

/* The number of elements of vertex_names */
static size_t vertex_names_len;

/* Drawing offset x */
static int offset_x;

/* Drawing offset x */
static int offset_y;

/* How wide (in pixels) a fatness 1 node should be */
static unsigned int base_node_radius = 8;

/* How much (in pixels) a node should widen for each fatness level */
static unsigned int node_radius_per_fatness = 7;

/* How wide (in pixels) the outline should be */
static int outline_width = 2;

/* How wide (in pixels) the arrowheads should be */
static int arrow_length = 7;

/* How narrow the arrowheads should be */
static double arrow_angle = 6.5 * M_PI / 8.0;

/* How close to an arrow (in pixels) for it to be selected */
static int edge_margin = 5;

/* If we're interacting with a vertex, which one */
static size_t selected_vertex = (size_t) -1;

/* If we're interacting with an edge, the i */
static size_t selected_edge_i = (size_t) -1;

/* If we're interacting with an edge, the j */
static size_t selected_edge_j = (size_t) -1;

/* If we're adding an edge, the last vertex we clicked on */
static size_t last_clicked_vertex = (size_t) -1;

/* If we're adding a new vertex, the number to use */
static uint16_t next_v_num = 1;

/* Memory for next vertex name */
static char next_v_name[32] = { 'v', '1', 0 };

/* x-coordinate of last mouse position */
static int last_mouse_x = -1;

/* y-coordinate of last mouse position */
static int last_mouse_y = -1;

/* Window title */
static char window_title[2048];

/* What name we last loaded/saved */
static char current_filename[2048] = { 'U', 'n', 't', 'i', 't', 'l', 'e', 'd',
                                       0 };

/* Whether we've edited since last loading/saving */
static uint_fast8_t current_file_edited = 0;

/* Maximum length of an input string we'll accept */
static size_t max_input_size = 1 << 10;

/* Current position in input */
static size_t input_idx;

/* Current length of string held in input */
static size_t input_len;

/* If a string is being typed in, what it is (in UTF-8 as per SDL2) */
static char *input;

/* Input, with `Filename:' or `New name:' prepended (for rendering) */
static char *input_with_prefix;

/* Texture for what user is currently entering */
static SDL_Texture *input_texture;

/* Whether we need to re-render the input */
static uint_fast8_t rerender_input_string;

/* To prevent awkward repeats in SDL events, rate-limit `s' and `l' */
static uint_fast8_t save_load_delay;

/* If something needs to be freed next frame, but not before */
static void *free_this;

/* GCD */
static int
gcd(uint_fast8_t x, uint_fast8_t y)
{
        uint_fast8_t r = 0;

        if (!x &&
            !y) {
                return 1;
        }

        while (y) {
                r = x % y;
                x = y;
                y = r;
        }

        return x;
}

/* Hex to uint */
static int
decode_hex(char *s, uint32_t *out_argb)
{
        uint32_t z = 0;

        while (*s &&
               (*s == ' ' ||
                *s == '#')) {
                s++;
        }

        if (strlen(s) != 6) {
                return -1;
        }

        while (*s) {
                z <<= 4;

                if ('0' <= *s &&
                    *s <= '9') {
                        z |= (*s - '0');
                } else if ('a' <= *s &&
                           *s <= 'f') {
                        z |= (*s - 'a' + 10);
                } else if ('A' <= *s &&
                           *s <= 'F') {
                        z |= (*s - 'A' + 10);
                } else {
                        return -1;
                }

                s++;
        }

        *out_argb = z;

        return 0;
}

/* Allocate and print a rational in the way a user expects */
static int
pretty_fraction(struct rational *r, char **out)
{
        int ret = 0;

        if (r->q == 1) {
                size_t len = snprintf(0, 0, "%d", (int) r->p);

                if (len + 1 < len) {
                        ret = errno = EOVERFLOW;
                        perror(L(""));
                        goto done;
                }

                if (!(*out = malloc(len + 1))) {
                        ret = errno;
                        perror(L("malloc"));
                        goto done;
                }

                sprintf(*out, "%d", (int) r->p);
                goto done;
        }

        size_t len = snprintf(0, 0, "%d/%u", (int) r->p, (unsigned int) r->q);

        if (len + 1 < len) {
                ret = errno = EOVERFLOW;
                perror(L(""));
                goto done;
        }

        if (!(*out = malloc(len + 1))) {
                ret = errno;
                perror(L("malloc"));
                goto done;
        }

        sprintf(*out, "%d/%u", (int) r->p, (unsigned int) r->q);
done:

        return ret;
}

/* Render text to texture */
static int
render_text(const char *text, SDL_Texture **out)
{
        if (!out) {
                return 0;
        }

        if (*out) {
                SDL_DestroyTexture(*out);
        }

        int ret = 0;
        SDL_Surface *surf = 0;

        if (!(surf = TTF_RenderUTF8_Blended_Wrapped(normal_font, text,
                                                    color_font, 800))) {
                fprintf(stderr, L("TTF_RenderUTF8_Shaded(): %s\n"),
                        TTF_GetError());
                ret = EINVAL;
                goto done;
        }

        if (!(*out = SDL_CreateTextureFromSurface(sdl_renderer, surf))) {
                fprintf(stderr, L("SDL_CreateTextureFromSurface(): %s\n"),
                        SDL_GetError());
                ret = EINVAL;
                goto done;
        }

done:
        SDL_FreeSurface(surf);

        return ret;
}

/* Load fonts */
static int
load_fonts(void)
{
        int ret = 0;

        if (normal_font) {
                TTF_CloseFont(normal_font);
                normal_font = 0;
        }

        if (normal_font_rw) {
                SDL_RWclose(normal_font_rw);
                normal_font_rw = 0;
        }

        if (!(normal_font_rw = SDL_RWFromConstMem(ttf_memory,
                                                  sizeof_ttf_memory()))) {
                ret = EINVAL;
                fprintf(stderr, L("SDL_RWFromConstMem(): %s\n"),
                        SDL_GetError());
                goto done;
        }

        if (!(normal_font = TTF_OpenFontIndexRW(normal_font_rw, 0,
                                                base_font_size, 0))) {
                ret = EINVAL;
                fprintf(stderr, L("TTF_OpenFont(): %s\n"), TTF_GetError());
                goto done;
        }

        for (size_t j = 0; j < UIA_LEN; ++j) {
                if ((ret = render_text(bottom_string[j], &bottom_text[j]))) {
                        goto done;
                }
        }

done:

        if (ret) {
                if (normal_font) {
                        TTF_CloseFont(normal_font);
                }

                if (normal_font_rw) {
                        SDL_RWclose(normal_font_rw);
                }

                normal_font_rw = 0;
                normal_font = 0;
        }

        return ret;
}

/* Insert str into input at given position */
static void
text_input(const char *str)
{
        size_t l = strlen(str);

        if (l + input_len + 1 >= max_input_size) {
                return;
        }

        for (size_t k = input_len; k > input_idx; --k) {
                input[k + l] = input[k];
        }

        /* Special-case for input_idx: 0 prohibits `>=' above */
        input[input_idx + l] = input[input_idx];

        for (size_t k = 0; k < l; ++k) {
                input[input_idx + k] = str[k];
        }

        input_idx += l;
        input_len += l;
        rerender_input_string = 1;
}

/* Move input cursor left */
static void
text_input_left(void)
{
        if (input_idx > 0) {
                input_idx--;
        }

        while (input_idx > 0) {
                unsigned char c = input[input_idx];

                if ((c & 0xc0) == 0x80) {
                        input_idx--;
                } else {
                        break;
                }
        }
}

/* Move input cursor right */
static void
text_input_right(void)
{
        size_t adv = 1;
        unsigned char c = input[input_idx];

        if ((c & 0x80) == 0x0) {
                adv = 1;
        } else if ((c & 0xe0) == 0xc0) {
                adv = 2;
        } else if ((c & 0xf0) == 0xe0) {
                adv = 3;
        } else if ((c & 0xf8) == 0xf0) {
                adv = 4;
        }

        if (input_idx + adv <= input_len) {
                input_idx += adv;
        }
}

/* Backspace */
static void
text_input_bs(void)
{
        if (!input_idx) {
                return;
        }

        size_t bs = 1;

        while (input_idx > bs) {
                unsigned char c = input[input_idx - bs];

                if ((c & 0xc0) == 0x80) {
                        bs++;
                } else {
                        break;
                }
        }

        for (size_t k = input_idx - bs; k <= input_len - bs; k++) {
                input[k] = input[k + bs];
        }

        input_len -= bs;
        input_idx -= bs;
        rerender_input_string = 1;
}

/* Set the title to something like `clav-sdl - foobar.txt*' */
static void
update_window_title(void)
{
        size_t cutoff = sizeof (window_title) / sizeof (window_title[0]);
        size_t r = snprintf(window_title, cutoff, "clav-sdl - %s%s",
                            current_filename, (current_file_edited ? "*" : ""));

        if (r >= cutoff) {
                window_title[cutoff - 1] = '\0';
        }

        SDL_SetWindowTitle(sdl_win, window_title);
}

/* Make sure the window title displays as edited */
static void
mark_as_edited(void)
{
        if (current_file_edited) {
                return;
        }

        current_file_edited = 1;
        update_window_title();
}

/* Convert `internal coordinates' to pixel coordinates */
static void
internal_to_pixel_xy(int in_x, int in_y, int *out_x, int *out_y)
{
        *out_x = in_x + offset_x;
        *out_y = in_y + offset_y;
}

/* Convert pixel coordinates to `internal coordinates' */
static void
pixel_to_internal_xy(int in_x, int in_y, int *out_x, int *out_y)
{
        *out_x = in_x - offset_x;
        *out_y = in_y - offset_y;
}

/* Set selected_vertex and selected_edge_{i,j} */
static int
recalculate_selected_items(int mx, int my)
{
        int x = 0;
        int y = 0;
        int ret = 0;
        char *s = 0;
        char *sij = 0;
        char *sji = 0;

        pixel_to_internal_xy(mx, my, &x, &y);
        size_t last_vertex = selected_vertex;
        size_t last_edge_i = selected_edge_i;
        size_t last_edge_j = selected_edge_j;

        selected_vertex = (size_t) -1;
        selected_edge_i = (size_t) -1;
        selected_edge_j = (size_t) -1;

        if (last_vertex != (size_t) -1) {
                struct vertex *v = &(q->v[last_vertex]);
                int r = base_node_radius + v->fatness * node_radius_per_fatness;

                if (x > v->x - r &&
                    x < v->x + r &&
                    y > v->y - r &&
                    y < v->y + r) {
                        selected_vertex = last_vertex;
                        goto compute_str;
                }
        }

        for (size_t j = q->v_num; j > 0; --j) {
                struct vertex *v = &(q->v[j - 1]);
                int r = base_node_radius + v->fatness * node_radius_per_fatness;

                if (x > v->x - r &&
                    x < v->x + r &&
                    y > v->y - r &&
                    y < v->y + r) {
                        selected_vertex = j - 1;
                        goto compute_str;
                }
        }

        for (size_t j = 1; j < q->v_num; ++j) {
                struct vertex *v1 = &(q->v[j]);

                for (size_t i = 0; i < j; ++i) {
                        if (!q->e[i * q->v_len + j].p &&
                            !q->e[j * q->v_len + i].p) {
                                continue;
                        }

                        struct vertex *v2 = &(q->v[i]);

                        if ((x - edge_margin > v1->x &&
                             x - edge_margin > v2->x) ||
                            (x + edge_margin < v1->x &&
                             x + edge_margin < v2->x) ||
                            (y - edge_margin > v1->y &&
                             y - edge_margin > v2->y) ||
                            (y + edge_margin < v1->y &&
                             y + edge_margin < v2->y)) {
                                continue;
                        }

                        if (v1->x == v2->x) {
                                if (x + edge_margin > v1->x &&
                                    x - edge_margin < v1->x) {
                                        selected_edge_i = i;
                                        selected_edge_j = j;
                                        goto compute_str;
                                }
                        } else if (v1->y == v2->y) {
                                if (y + edge_margin > v1->y &&
                                    y - edge_margin < v1->y) {
                                        selected_edge_i = i;
                                        selected_edge_j = j;
                                        goto compute_str;
                                }
                        } else {
                                double m1 = ((double) (v2->y - v1->y)) /
                                            ((double) (v2->x - v1->x));
                                double m2 = -1.0 / m1;
                                double xint = ((double) y - (double) v1->y +
                                               m1 * v1->x - m2 * x) / (m1 - m2);
                                double yint = m1 * xint - m1 * v1->x +
                                              (double) v1->y;

                                if ((x - xint) * (x - xint) + (y - yint) * (y -
                                                                            yint)
                                    < edge_margin * edge_margin) {
                                        selected_edge_i = i;
                                        selected_edge_j = j;
                                        goto compute_str;
                                }
                        }
                }
        }

compute_str:

        if (selected_vertex != (size_t) -1 &&
            (selected_vertex != last_vertex ||
             dragged_selected_vertex)) {
                dragged_selected_vertex = 0;
                struct vertex *v = &(q->v[selected_vertex]);
                size_t len = snprintf(0, 0,
                                      "Name:     %s\nFatness:  %d\nPosition: (%d,%d)",
                                      v->name,
                                      (int) v->fatness, v->x, v->y);

                if (len + 1 < len) {
                        ret = errno = EOVERFLOW;
                        perror(L(""));
                        goto done;
                }

                if (!(s = malloc(len + 1))) {
                        ret = errno;
                        perror(L("malloc"));
                        goto done;
                }

                sprintf(s, "Name:     %s\nFatness:  %d\nPosition: (%d,%d)",
                        v->name, (int) v->fatness, v->x, v->y);

                if ((ret = render_text(s, &selected_info))) {
                        goto done;
                }
        } else if ((selected_edge_i != last_edge_i ||
                    selected_edge_j != last_edge_j) &&
                   selected_edge_i != (size_t) -1 &&
                   selected_edge_j != (size_t) -1) {
                struct vertex *i = &(q->v[selected_edge_i]);
                struct vertex *j = &(q->v[selected_edge_j]);
                struct rational *eij = &(q->e[selected_edge_i * q->v_len +
                                              selected_edge_j]);
                struct rational *eji = &(q->e[selected_edge_j * q->v_len +
                                              selected_edge_i]);

                if ((ret = pretty_fraction(eij, &sij))) {
                        goto done;
                }

                if ((ret = pretty_fraction(eji, &sji))) {
                        goto done;
                }

                size_t len = snprintf(0, 0,
                                      "%s \u2192 %s: %s\n%s \u2192 %s: %s",
                                      i->name, j->name, sij,
                                      j->name, i->name, sji);

                if (len + 1 < len) {
                        ret = errno = EOVERFLOW;
                        perror(L(""));
                        goto done;
                }

                if (!(s = malloc(len + 1))) {
                        ret = errno;
                        perror(L("malloc"));
                        goto done;
                }

                sprintf(s, "%s \u2192 %s: %s\n%s \u2192 %s: %s", i->name,
                        j->name, sij, j->name, i->name, sji);

                if ((ret = render_text(s, &selected_info))) {
                        goto done;
                }
        }

done:
        free(s);
        free(sij);
        free(sji);

        return ret;
}

/* Render vertex names as textures */
static int
render_vertex_names(void)
{
        if (vertex_names) {
                for (size_t j = 0; j < vertex_names_len; ++j) {
                        SDL_DestroyTexture(vertex_names[j]);
                        vertex_names[j] = 0;
                }
        }

        if (!q->v_num) {
                return 0;
        }

        if ((q->v_num * sizeof *vertex_names) / q->v_num !=
            sizeof *vertex_names) {
                errno = EOVERFLOW;
                perror(L(""));
                vertex_names_len = 0;

                return EOVERFLOW;
        }

        if (!(vertex_names = realloc(vertex_names, q->v_num *
                                     sizeof(*vertex_names)))) {
                int sv_err = errno;

                perror(L("realloc"));
                vertex_names_len = 0;

                return sv_err;
        }

        vertex_names_len = q->v_num;

        for (size_t j = 0; j < vertex_names_len; ++j) {
                vertex_names[j] = 0;
        }

        for (size_t j = 0; j < vertex_names_len; ++j) {
                int ret = 0;

                if ((ret = render_text(q->v[j].name, &(vertex_names[j])))) {
                        return ret;
                }
        }

        return 0;
}

/* Get information about the window */
static void
react_to_window_resized(void)
{
        int old_pix_width = da_pix_width;
        int old_pix_height = da_pix_height;

        SDL_GetWindowSize(sdl_win, &da_pix_width, &da_pix_height);

        if (old_pix_width == da_pix_width &&
            old_pix_height == da_pix_height) {
                return;
        }

        offset_x += (da_pix_width - old_pix_width) / 2;
        offset_y += (da_pix_height - old_pix_height) / 2;
}

/* Pop from queue */
static void
eq_pop(struct ui_event *out)
{
        if (eq_head == eq_tail) {
                *out = (struct ui_event) { 0 };

                return;
        }

        memcpy(out, eq_buf + eq_head, sizeof *out);
        eq_buf[eq_head] = (struct ui_event) { 0 };
        eq_head = (eq_head + 1) % eq_len;
}

/* Push into queue */
static int
eq_push(struct ui_event *in)
{
        if (((eq_tail + 1) % eq_len) == eq_head) {
                void *newmem;

                if ((eq_len * sizeof *in) >= ((size_t) -1) / 2) {
                        fprintf(stderr, L(
                                        "eq_push: Impossibly large buffer\n"));

                        return ENOMEM;
                }

                if ((eq_len * 2) / 2 != eq_len ||
                    (eq_len * 2 * sizeof *eq_buf) / (eq_len * 2) !=
                    sizeof *eq_buf) {
                        errno = EOVERFLOW;
                        perror(L(""));

                        return EOVERFLOW;
                }

                if (!(newmem = realloc(eq_buf, (eq_len * 2) *
                                       sizeof *eq_buf))) {
                        int sv_err = errno;

                        perror(L("realloc"));

                        return sv_err;
                }

                eq_buf = (struct ui_event *) newmem;
                eq_len *= 2;
        }

        memcpy(eq_buf + eq_tail, in, sizeof *in);
        eq_tail = (eq_tail + 1) % eq_len;

        return 0;
}

/* Initialize SDL */
int
ui_init(struct quiver *i_q)
{
        int ret;

        q = i_q;
        size_t padding = strlen("Filename: ");

        /* No need for overflow check, max_input_size is capped */
        if (!(input_with_prefix = malloc(max_input_size + padding))) {
                ret = errno;
                perror(L("malloc"));

                return ret;
        }

        strcpy(input_with_prefix, "Filename: ");
        input = input_with_prefix + padding;
        input_idx = 0;
        input_len = 0;

        if (SDL_Init(SDL_INIT_VIDEO) < 0) {
                fprintf(stderr, L("SDL_Init(): %s\n"), SDL_GetError());

                return ENXIO;
        }

        sdl_win = SDL_CreateWindow("clav-sdl", SDL_WINDOWPOS_UNDEFINED,
                                   SDL_WINDOWPOS_UNDEFINED, 1000, 1000,
                                   SDL_WINDOW_SHOWN |
                                   SDL_WINDOW_RESIZABLE);

        if (!sdl_win) {
                fprintf(stderr, L("SDL_CreateWindow(): %s\n"), SDL_GetError());

                return EINVAL;
        }

        sdl_renderer = SDL_CreateRenderer(sdl_win, -1,
                                          SDL_RENDERER_ACCELERATED);

        if (!sdl_renderer) {
                fprintf(stderr, L("SDL_CreateRenderer(): %s\n"),
                        SDL_GetError());

                return EINVAL;
        }

        if (TTF_Init() < 0) {
                fprintf(stderr, L("TTF_Init(): %s\n"), TTF_GetError());

                return EINVAL;
        }

        if ((ret = load_fonts())) {
                goto done;
        }

        if ((ret = render_vertex_names())) {
                goto done;
        }

        if ((ret = SDL_SetRenderDrawColor(sdl_renderer, color_bg.r, color_bg.g,
                                          color_bg.b, color_bg.a))) {
                fprintf(stderr, L("SDL_SetRenderDrawColor(): %s\n"),
                        SDL_GetError());
                goto done;
        }

        if ((ret = SDL_RenderClear(sdl_renderer))) {
                fprintf(stderr, L("SDL_RenderClear(): %s\n"), SDL_GetError());
                goto done;
        }

        update_window_title();
        SDL_RenderPresent(sdl_renderer);
        react_to_window_resized();

        /* Set up queue for returning data */
        if (!(eq_buf = calloc(2, sizeof *eq_buf))) {
                ret = errno;
                perror(L("calloc"));
                goto done;
        }

        eq_len = 2;
        eq_head = 0;
        eq_tail = 0;
done:

        return ret;
}

/* Deal with the fact that the quiver was changed */
int
ui_respond_quiver_change(void)
{
        redraw = 4;
        return render_vertex_names();
}

/* Acknowledge a successful load */
int
ui_respond_successful_load(const char *filename)
{
        const char *tail = (const char *) strrchr(filename, '/');

        if (!tail) {
                tail = filename;
        } else {
                tail++;
        }

        size_t cutoff = sizeof(current_filename) / sizeof(current_filename[0]);
        size_t r = snprintf(current_filename, cutoff, "%s", tail);

        if (r >= cutoff) {
                current_filename[cutoff - 1] = '\0';
        }

        current_file_edited = 0;
        next_v_num = 1;
        update_window_title();

        return 0;
}

/* Acknowledge a successful save */
int
ui_respond_successful_save(const char *filename)
{
        return ui_respond_successful_load(filename);
}

/* Tear down SDL */
int
ui_teardown(void)
{
        if (vertex_names) {
                for (size_t j = 0; j < vertex_names_len; ++j) {
                        SDL_DestroyTexture(vertex_names[j]);
                        vertex_names[j] = 0;
                }

                free(vertex_names);
                vertex_names = 0;
        }

        for (size_t j = 0; j < UIA_LEN; ++j) {
                SDL_DestroyTexture(bottom_text[j]);
                bottom_text[j] = 0;
        }

        if (selected_info) {
                SDL_DestroyTexture(selected_info);
                selected_info = 0;
        }

        if (input_texture) {
                SDL_DestroyTexture(input_texture);
                input_texture = 0;
        }

        if (normal_font) {
                TTF_CloseFont(normal_font);
                normal_font = 0;
        }

        if (normal_font_rw) {
                SDL_RWclose(normal_font_rw);
                normal_font_rw = 0;
        }

        if (sdl_win) {
                SDL_DestroyWindow(sdl_win);
                sdl_win = 0;
        }

        free(eq_buf);
        eq_buf = 0;
        free(input_with_prefix);
        input_with_prefix = 0;
        input = 0;
        SDL_Quit();

        return 0;
}

/* Record that a frame has been started */
int
ui_start_frame(void)
{
        int ret = 0;
        int rho = 0;
        SDL_Rect r = { 0 };
        Uint32 dummy_format;
        int dummy_access;
        int tex_w;
        int tex_h;

        frame_start_ticks = SDL_GetTicks();

        if (!redraw) {
                goto done;
        }

        redraw--;

        /* Draw the damn thing */
        SDL_SetRenderDrawColor(sdl_renderer, color_bg.r, color_bg.g, color_bg.b,
                               color_bg.a);
        SDL_RenderClear(sdl_renderer);

        /* Special case for if text input is going on */
        if (rerender_input_string) {
                rerender_input_string = 0;

                if ((ret = render_text(input_with_prefix, &input_texture))) {
                        goto done;
                }
        }

        /* Draw each edge */
        for (size_t j = 0; j < q->v_num; ++j) {
                for (size_t i = 0; i < j; ++i) {
                        /* First, determine if we're looking at a half-edge or a full-edge */
                        int d = gcd(q->v[i].fatness, q->v[j].fatness);
                        struct rational *eij = &(q->e[i * q->v_len + j]);
                        struct rational *eji = &(q->e[j * q->v_len + i]);
                        int cx = 0;
                        int cy = 0;
                        int cx2 = 0;
                        int cy2 = 0;
                        double theta = 0.0;

                        if (!eij->p &&
                            !eji->p) {
                                continue;
                        }

                        internal_to_pixel_xy(q->v[i].x, q->v[i].y, &cx, &cy);
                        internal_to_pixel_xy(q->v[j].x, q->v[j].y, &cx2, &cy2);

                        if (selected_edge_i == i &&
                            selected_edge_j == j) {
                                if ((ret = SDL_SetRenderDrawColor(sdl_renderer,
                                                                  color_e_sel.r,
                                                                  color_e_sel.g,
                                                                  color_e_sel.b,
                                                                  color_e_sel.a)))
                                {
                                        fprintf(stderr, L(
                                                        "SDL_RenderDrawColor(): %s\n"),
                                                SDL_GetError());
                                        goto done;
                                }

                                for (int id = -edge_margin; id < edge_margin;
                                     ++id) {
                                        for (int jd = -edge_margin; jd <
                                             edge_margin; ++jd) {
                                                if ((ret = SDL_RenderDrawLine(
                                                             sdl_renderer, cx +
                                                             id, cy + jd,
                                                             cx2 + id, cy2 +
                                                             jd))) {
                                                        fprintf(stderr, L(
                                                                        "SDL_RenderDrawLine(): %s\n"),
                                                                SDL_GetError());
                                                        goto done;
                                                }
                                        }
                                }
                        }

                        /* This is the (eij)/dj = -(eji)/di condition */
                        if (eij->p * q->v[i].fatness * eji->q != -eji->p *
                            q->v[j].fatness * eij->q) {
                                ret = SDL_SetRenderDrawColor(sdl_renderer,
                                                             color_e_abnormal.r,
                                                             color_e_abnormal.g,
                                                             color_e_abnormal.b,
                                                             color_e_abnormal.a);
                        } else if ((int) (abs(eij->p) * d) ==
                                   (int) (q->v[j].fatness * eij->q)) {
                                ret = SDL_SetRenderDrawColor(sdl_renderer,
                                                             color_e_normal.r,
                                                             color_e_normal.g,
                                                             color_e_normal.b,
                                                             color_e_normal.a);
                        } else if ((int) (2 * abs(eij->p) * d) ==
                                   (int) (q->v[j].fatness * eij->q)) {
                                ret = SDL_SetRenderDrawColor(sdl_renderer,
                                                             color_e_half.r,
                                                             color_e_half.g,
                                                             color_e_half.b,
                                                             color_e_half.a);
                        } else {
                                ret = SDL_SetRenderDrawColor(sdl_renderer,
                                                             color_e_abnormal.r,
                                                             color_e_abnormal.g,
                                                             color_e_abnormal.b,
                                                             color_e_abnormal.a);
                        }

                        if (ret) {
                                fprintf(stderr, L(
                                                "SDL_SetRenderDrawColor(): %s\n"),
                                        SDL_GetError());
                                goto done;
                        }

                        if ((ret = SDL_RenderDrawLine(sdl_renderer, cx, cy, cx2,
                                                      cy2))) {
                                fprintf(stderr, L("SDL_RenderDrawLine(): %s\n"),
                                        SDL_GetError());
                                goto done;
                        }

                        if (cx == cx2) {
                                theta = (cy2 > cy) ? M_PI / 2.0 : -M_PI / 2.0;
                        } else {
                                theta = atan2f(cy2 - cy, cx2 - cx);
                        }

                        if ((eij->p < 0)) {
                                theta += M_PI;
                        }

                        cx = (cx + cx2) / 2;
                        cy = (cy + cy2) / 2;
                        cx2 = cx + arrow_length * cos(theta + arrow_angle);
                        cy2 = cy + arrow_length * sin(theta + arrow_angle);

                        if ((ret = SDL_RenderDrawLine(sdl_renderer, cx, cy, cx2,
                                                      cy2))) {
                                fprintf(stderr, L("SDL_RenderDrawLine(): %s\n"),
                                        SDL_GetError());
                                goto done;
                        }

                        if ((ret = SDL_RenderDrawLine(sdl_renderer, cx, cy, cx +
                                                      arrow_length * cos(theta -
                                                                         arrow_angle),
                                                      cy +
                                                      arrow_length * sin(theta -
                                                                         arrow_angle))))
                        {
                                fprintf(stderr, L("SDL_RenderDrawLine(): %s\n"),
                                        SDL_GetError());
                                goto done;
                        }
                }
        }

        /* Draw each vertex as a box */
        for (size_t j = 0; j < q->v_num; ++j) {
                struct vertex *v = &(q->v[j]);
                int cx = 0;
                int cy = 0;

                internal_to_pixel_xy(v->x, v->y, &cx, &cy);

                /* Central square */
                SDL_SetRenderDrawBlendMode(sdl_renderer, SDL_BLENDMODE_NONE);
                SDL_SetRenderDrawColor(sdl_renderer, v->r, v->g, v->b, 0xff);
                rho = base_node_radius + node_radius_per_fatness * v->fatness;
                r.x = cx - rho;
                r.y = cy - rho;
                r.w = 2 * rho;
                r.h = 2 * rho;
                SDL_RenderFillRect(sdl_renderer, &r);

                /* Outline */
                SDL_SetRenderDrawBlendMode(sdl_renderer, SDL_BLENDMODE_BLEND);

                if (j == selected_vertex ||
                    j == last_clicked_vertex) {
                        SDL_SetRenderDrawColor(sdl_renderer,
                                               color_outline_sel.r,
                                               color_outline_sel.g,
                                               color_outline_sel.b,
                                               color_outline_sel.a);
                } else {
                        SDL_SetRenderDrawColor(sdl_renderer, color_outline.r,
                                               color_outline.g, color_outline.b,
                                               color_outline.a);
                }

                r.x = cx - rho;
                r.y = cy - rho;
                r.w = 2 * rho - outline_width;
                r.h = outline_width;
                SDL_RenderFillRect(sdl_renderer, &r);
                r.x = cx + rho - outline_width;
                r.y = cy - rho;
                r.w = outline_width;
                r.h = 2 * rho - outline_width;
                SDL_RenderFillRect(sdl_renderer, &r);
                r.x = cx - rho + outline_width;
                r.y = cy + rho - outline_width;
                r.w = 2 * rho - outline_width;
                r.h = outline_width;
                SDL_RenderFillRect(sdl_renderer, &r);
                r.x = cx - rho;
                r.y = cy - rho + outline_width;
                r.w = outline_width;
                r.h = 2 * rho - outline_width;
                SDL_RenderFillRect(sdl_renderer, &r);

                /* Text */
                if (j >= vertex_names_len) {
                        fprintf(stderr, L(
                                        "render_vertex_names() was not called, somehow\n"));
                        ret = EINVAL;
                        goto done;
                }

                if (SDL_QueryTexture(vertex_names[j], &dummy_format,
                                     &dummy_access, &tex_w, &tex_h)) {
                        fprintf(stderr, L("SDL_QueryTexture(): %s\n"),
                                SDL_GetError());
                        ret = EINVAL;
                        goto done;
                }

                r.x = cx - tex_w / 2;
                r.y = cy - tex_h / 2;
                r.w = tex_w;
                r.h = tex_h;
                SDL_RenderCopy(sdl_renderer, vertex_names[j], 0, &r);
        }

        /* If adding a new vertex, draw preview */
        if (ui_action == UIA_NEW_VERTEX &&
            last_mouse_x != -1 &&
            last_mouse_y != -1) {
                /* Central square */
                SDL_SetRenderDrawBlendMode(sdl_renderer, SDL_BLENDMODE_NONE);
                SDL_SetRenderDrawColor(sdl_renderer, color_v_preview.r,
                                       color_v_preview.g, color_v_preview.b,
                                       color_v_preview.a);
                rho = base_node_radius;
                r.x = last_mouse_x - rho;
                r.y = last_mouse_y - rho;
                r.w = 2 * rho;
                r.h = 2 * rho;
                SDL_RenderFillRect(sdl_renderer, &r);

                /* Outline */
                SDL_SetRenderDrawBlendMode(sdl_renderer, SDL_BLENDMODE_BLEND);
                SDL_SetRenderDrawColor(sdl_renderer, color_outline_preview.r,
                                       color_outline_preview.g,
                                       color_outline_preview.b,
                                       color_outline_preview.a);
                r.x = last_mouse_x - rho;
                r.y = last_mouse_y - rho;
                r.w = 2 * rho - outline_width;
                r.h = outline_width;
                SDL_RenderFillRect(sdl_renderer, &r);
                r.x = last_mouse_x + rho - outline_width;
                r.y = last_mouse_y - rho;
                r.w = outline_width;
                r.h = 2 * rho - outline_width;
                SDL_RenderFillRect(sdl_renderer, &r);
                r.x = last_mouse_x - rho + outline_width;
                r.y = last_mouse_y + rho - outline_width;
                r.w = 2 * rho - outline_width;
                r.h = outline_width;
                SDL_RenderFillRect(sdl_renderer, &r);
                r.x = last_mouse_x - rho;
                r.y = last_mouse_y - rho + outline_width;
                r.w = outline_width;
                r.h = 2 * rho - outline_width;
                SDL_RenderFillRect(sdl_renderer, &r);
        }

        /* If adding a new edge, draw possible */
        if ((ui_action == UIA_NEW_EDGE_2 ||
             ui_action == UIA_NEW_H_EDGE_2) &&

            /* last_clicked_vertex != (size_t) -1 && */
            last_mouse_x != -1 &&
            last_mouse_y != -1) {
                int cx = 0;
                int cy = 0;

                if ((ret = SDL_SetRenderDrawColor(sdl_renderer,
                                                  color_e_normal.r,
                                                  color_e_normal.g,
                                                  color_e_normal.b,
                                                  color_e_normal.a))) {
                        fprintf(stderr, L("SDL_RenderDrawLine(): %s\n"),
                                SDL_GetError());
                        goto done;
                }

                internal_to_pixel_xy(q->v[last_clicked_vertex].x,
                                     q->v[last_clicked_vertex].y, &cx, &cy);

                if ((ret = SDL_RenderDrawLine(sdl_renderer, cx, cy,
                                              last_mouse_x, last_mouse_y))) {
                        fprintf(stderr, L("SDL_RenderDrawLine(): %s\n"),
                                SDL_GetError());
                        goto done;
                }
        }

        /* Bottom text */
        if (SDL_QueryTexture(bottom_text[ui_action], &dummy_format,
                             &dummy_access, &tex_w, &tex_h)) {
                fprintf(stderr, L("SDL_QueryTexture(): %s\n"), SDL_GetError());
                ret = EINVAL;
                goto done;
        }

        r.x = text_border_padding;
        r.y = da_pix_height - tex_h - text_border_padding;
        r.w = tex_w;
        r.h = tex_h;
        SDL_RenderCopy(sdl_renderer, bottom_text[ui_action], 0, &r);

        /* If something is selected */
        if (selected_info &&
            (selected_vertex != (size_t) -1 ||
             (selected_edge_i != (size_t) -1 &&
              selected_edge_j != (size_t) -1))) {
                if (SDL_QueryTexture(selected_info, &dummy_format,
                                     &dummy_access, &tex_w, &tex_h)) {
                        fprintf(stderr, L("SDL_QueryTexture(): %s\n"),
                                SDL_GetError());
                        ret = EINVAL;
                        goto done;
                }

                r.x = text_border_padding;
                r.y = text_border_padding;
                r.w = tex_w;
                r.h = tex_h;
                SDL_RenderCopy(sdl_renderer, selected_info, 0, &r);
        }

        /* If user is entering text */
        if (ui_action == UIA_ENTER_SAVE ||
            ui_action == UIA_ENTER_LOAD ||
            ui_action == UIA_ENTER_RENAME ||
            ui_action == UIA_ENTER_COLOR) {
                if (SDL_QueryTexture(bottom_text[ui_action], &dummy_format,
                                     &dummy_access, &tex_w, &tex_h)) {
                        fprintf(stderr, L("SDL_QueryTexture(): %s\n"),
                                SDL_GetError());
                        ret = EINVAL;
                        goto done;
                }

                r.x = text_border_padding;
                r.y = da_pix_height - tex_h - text_border_padding;

                if (SDL_QueryTexture(input_texture, &dummy_format,
                                     &dummy_access, &tex_w, &tex_h)) {
                        fprintf(stderr, L("SDL_QueryTexture(): %s\n"),
                                SDL_GetError());
                        ret = EINVAL;
                        goto done;
                }

                r.y -= (tex_h + text_border_padding);
                r.w = tex_w;
                r.h = tex_h;
                SDL_RenderCopy(sdl_renderer, input_texture, 0, &r);

                /* Now draw a cursor */
                char store_idxchar = input[input_idx];

                input[input_idx] = '\0';

                if ((ret = TTF_SizeUTF8(normal_font, input_with_prefix, &tex_w,
                                        &tex_h))) {
                        fprintf(stderr, L("TTF_SizeText(): %s\n"),
                                TTF_GetError());
                        goto done;
                }

                input[input_idx] = store_idxchar;

                if ((ret = SDL_SetRenderDrawColor(sdl_renderer, color_font.r,
                                                  color_font.g, color_font.b,
                                                  color_font.a))) {
                        fprintf(stderr, L("SDL_SetRenderDrawColor(): %s\n"),
                                SDL_GetError());
                        goto done;
                }

                if ((ret = SDL_RenderDrawLine(sdl_renderer, r.x + tex_w, r.y,
                                              r.x + tex_w, r.y + tex_h))) {
                        fprintf(stderr, L("SDL_RenderDrawLine(): %s\n"),
                                SDL_GetError());
                        goto done;
                }
        }

done:

        return ret;
}

/* Draw a frame, possibly sleeping for framelimit */
int
ui_finish_frame(void)
{
        int ret = 0;
        struct ui_event ui_e = { 0 };
        SDL_Event sdl_e = { 0 };
        Uint32 now = 0;
        uint_fast8_t save_requested = 0;
        uint_fast8_t load_requested = 0;
        int color_ret = 0;
        uint_fast8_t color_affirmed = 0;

        if (free_this) {
                free(free_this);
                free_this = 0;
        }

        if (save_load_delay) {
                save_load_delay--;
        }

        SDL_RenderPresent(sdl_renderer);

        /* Handle user input */
        while (SDL_PollEvent(&sdl_e) != 0) {
                /*
                 * Redraw should be at least 2, to deal with
                 * double-buffering. Triple-buffering is occasionally
                 * used, let's go with 4 to be safe.
                 */
                redraw = 4;
                SDL_Keycode k = 0;
                SDL_Keymod m = 0;

                switch (sdl_e.type) {
                case SDL_QUIT:
                        ui_e = (struct ui_event) { .type = ET_QUIT };
                        ret = eq_push(&ui_e);
                        break;
                case SDL_TEXTINPUT:
                        text_input(sdl_e.text.text);
                        break;
                case SDL_KEYDOWN:

                        if (sdl_e.key.repeat) {
                                break;
                        }

                        k = sdl_e.key.keysym.sym;
                        m = sdl_e.key.keysym.mod;

                        switch (ui_action) {
                        case UIA_ENTER_SAVE:
                        case UIA_ENTER_LOAD:
                        case UIA_ENTER_RENAME:
                        case UIA_ENTER_COLOR:

                                if (k == SDLK_ESCAPE) {
                                        SDL_StopTextInput();
                                        ui_action = UIA_NONE;
                                        last_clicked_vertex = (size_t) -1;
                                } else if (k == SDLK_RETURN ||
                                           k == SDLK_RETURN2) {
                                        SDL_StopTextInput();
                                        ui_e = (struct ui_event) { .str =
                                                                           input };

                                        if (ui_action == UIA_ENTER_SAVE) {
                                                ui_e.type = ET_SAVE;
                                        } else if (ui_action ==
                                                   UIA_ENTER_LOAD) {
                                                ui_e.type = ET_LOAD;
                                        } else if (ui_action ==
                                                   UIA_ENTER_RENAME) {
                                                ui_e.type = ET_RENAME;
                                                ui_e.idx_1 =
                                                        last_clicked_vertex;
                                        } else if (ui_action ==
                                                   UIA_ENTER_COLOR) {
                                                ui_e.type = ET_CHANGE_COLOR;
                                                ui_e.idx_1 =
                                                        last_clicked_vertex;
                                                ui_e.str = 0;

                                                if (decode_hex(input,
                                                               &ui_e.z)) {
                                                        ui_action =
                                                                ui_action_loop ?
                                                                UIA_CHANGE_COLOR
                                                                :
                                                                UIA_NONE;
                                                        last_clicked_vertex =
                                                                (size_t) -1;
                                                        break;
                                                }
                                        }

                                        ret = eq_push(&ui_e);

                                        if (ui_action_loop) {
                                                if (ui_action ==
                                                    UIA_ENTER_RENAME) {
                                                        ui_action = UIA_RENAME;
                                                } else if (ui_action ==
                                                           UIA_ENTER_COLOR) {
                                                        ui_action =
                                                                UIA_CHANGE_COLOR;
                                                }
                                        } else {
                                                ui_action = UIA_NONE;
                                        }

                                        last_clicked_vertex = (size_t) -1;
                                } else if (k == SDLK_LEFT) {
                                        text_input_left();
                                } else if (k == SDLK_RIGHT) {
                                        text_input_right();
                                } else if (k == SDLK_BACKSPACE) {
                                        text_input_bs();
                                }

                                break;
                        case UIA_NONE:

                                if (k == SDLK_q) {
                                        ui_action = UIA_ASK_QUIT;
                                } else if (k == SDLK_d) {
                                        ui_action = UIA_DELETE;
                                        ui_action_loop = (m & KMOD_SHIFT);
                                } else if (k == SDLK_e) {
                                        ui_action = UIA_NEW_EDGE_1;
                                        ui_action_loop = (m & KMOD_SHIFT);
                                } else if (k == SDLK_f) {
                                        ui_action = UIA_INC_FATNESS;
                                        ui_action_loop = (m & KMOD_SHIFT);
                                } else if (k == SDLK_g) {
                                        ui_action = UIA_DEC_FATNESS;
                                        ui_action_loop = (m & KMOD_SHIFT);
                                } else if (k == SDLK_c) {
                                        ui_action = UIA_CHANGE_COLOR;
                                        ui_action_loop = (m & KMOD_SHIFT);
                                } else if (k == SDLK_h) {
                                        ui_action = UIA_NEW_H_EDGE_1;
                                        ui_action_loop = (m & KMOD_SHIFT);
                                } else if (k == SDLK_m) {
                                        ui_action = UIA_MUTATE;
                                        ui_action_loop = (m & KMOD_SHIFT);
                                } else if (k == SDLK_r) {
                                        ui_action = UIA_RENAME;
                                        ui_action_loop = (m & KMOD_SHIFT);
                                } else if (k == SDLK_v) {
                                        ui_action = UIA_NEW_VERTEX;
                                        ui_action_loop = (m & KMOD_SHIFT);
                                } else if (k == SDLK_l &&
                                           !save_load_delay) {
                                        /* Don't load - SDL_KEYDOWN repeats */
                                        load_requested = 1;
                                } else if (k == SDLK_s &&
                                           !save_load_delay) {
                                        save_requested = 1;
                                }

                                break;
                        case UIA_ASK_QUIT:

                                if (k == SDLK_n ||
                                    k == SDLK_ESCAPE) {
                                        ui_action = UIA_NONE;
                                } else if (k == SDLK_y) {
                                        ui_e = (struct ui_event) { .type =
                                                                           ET_QUIT };
                                        ret = eq_push(&ui_e);
                                        ui_action = UIA_NONE;
                                }

                                break;
                        default:

                                if (k == SDLK_q ||
                                    k == SDLK_ESCAPE) {
                                        ui_action = UIA_NONE;
                                }

                                break;
                        }

                        break;
                case SDL_WINDOWEVENT:

                        if (sdl_e.window.event == SDL_WINDOWEVENT_RESIZED ||
                            sdl_e.window.event == SDL_WINDOWEVENT_MAXIMIZED ||
                            sdl_e.window.event == SDL_WINDOWEVENT_RESTORED) {
                                react_to_window_resized();
                        } else if (sdl_e.window.event ==
                                   SDL_WINDOWEVENT_LEAVE) {
                                /* This tells the dragging code to not respond */
                                last_mouse_x = -1;
                                last_mouse_y = -1;
                        }

                        break;
                case SDL_MOUSEMOTION:

                        if (sdl_e.motion.state & SDL_BUTTON_LMASK) {
                                int x = sdl_e.motion.x;
                                int y = sdl_e.motion.y;

                                if (last_mouse_x >= 0 &&
                                    last_mouse_y >= 0) {
                                        if (selected_vertex != (size_t) -1) {
                                                q->v[selected_vertex].x += (x -
                                                                            last_mouse_x);
                                                q->v[selected_vertex].y += (y -
                                                                            last_mouse_y);
                                                dragged_selected_vertex = 1;
                                                recalculate_selected_items(
                                                        sdl_e.motion.x,
                                                        sdl_e.motion.y);
                                                mark_as_edited();
                                        } else {
                                                offset_x += (x - last_mouse_x);
                                                offset_y += (y - last_mouse_y);
                                        }
                                }
                        } else {
                                recalculate_selected_items(sdl_e.motion.x,
                                                           sdl_e.motion.y);
                        }

                        last_mouse_x = sdl_e.motion.x;
                        last_mouse_y = sdl_e.motion.y;
                        break;
                case SDL_MOUSEBUTTONUP:

                        if ((sdl_e.button.state & SDL_BUTTON_LMASK) &&
                            ui_action != UIA_NEW_VERTEX) {
                                last_mouse_x = -1;
                                last_mouse_y = -1;
                        }

                        recalculate_selected_items(sdl_e.button.x,
                                                   sdl_e.button.y);
                        break;
                case SDL_MOUSEBUTTONDOWN:

                        if (!(sdl_e.button.state & SDL_BUTTON_LMASK)) {
                                break;
                        }

                        if (ui_action != UIA_NEW_VERTEX) {
                                last_mouse_x = -1;
                                last_mouse_y = -1;
                        }

                        switch (ui_action) {
                        case UIA_MUTATE:

                                if (selected_vertex == (size_t) -1) {
                                        break;
                                }

                                ui_e = (struct ui_event) {
                                        /* */
                                        .type = ET_MUTATE, .idx_1 =
                                                selected_vertex
                                };
                                ret = eq_push(&ui_e);
                                ui_action = ui_action_loop ? UIA_MUTATE :
                                            UIA_NONE;
                                break;
                        case UIA_CHANGE_COLOR:

                                if (selected_vertex == (size_t) -1) {
                                        break;
                                }

                                ui_e = (struct ui_event) {
                                        /* */
                                        .type = ET_CHANGE_COLOR, .idx_1 =
                                                selected_vertex, .z = 0
                                };
                                color_ret = choose_color(&ui_e.z,
                                                         &color_affirmed);

                                if (!color_ret) {
                                        if (color_affirmed) {
                                                ret = eq_push(&ui_e);
                                        }

                                        ui_action = ui_action_loop ?
                                                    UIA_CHANGE_COLOR : UIA_NONE;
                                } else {
                                        ui_action = UIA_ENTER_COLOR;
                                        last_clicked_vertex = selected_vertex;
                                        input_idx = 0;
                                        input_len = 0;
                                        input[0] = '\0';
                                        SDL_StartTextInput();

                                        /* The nul terminator bleeds over into input */
                                        strncpy(input_with_prefix, "  rrggbb: ",
                                                11);
                                        rerender_input_string = 1;
                                }

                                break;
                        case UIA_NEW_VERTEX:

                                if (selected_vertex != (size_t) -1 ||
                                    selected_edge_i != (size_t) -1 ||
                                    selected_edge_j != (size_t) -1) {
                                        break;
                                }

                                int cx = sdl_e.button.x - offset_x;
                                int cy = sdl_e.button.y - offset_y;

                                sprintf(next_v_name, "v%zu",
                                        (size_t) next_v_num);
                                next_v_num++;
                                ui_e = (struct ui_event) {
                                        /* */
                                        .type = ET_NEW_VERTEX, .int_1 = cx,
                                        .int_2 = cy, .z = (color_v.r << 16) |
                                                          (color_v.g << 8) |
                                                          color_v.b, .str =
                                                next_v_name,
                                };
                                ret = eq_push(&ui_e);
                                ui_action = ui_action_loop ? UIA_NEW_VERTEX :
                                            UIA_NONE;
                                break;
                        case UIA_NEW_EDGE_1:
                        case UIA_NEW_H_EDGE_1:
                        case UIA_RENAME:

                                if (selected_vertex == (size_t) -1) {
                                        if (!ui_action_loop) {
                                                ui_action = UIA_NONE;
                                        }

                                        break;
                                }

                                last_clicked_vertex = selected_vertex;

                                if (ui_action == UIA_NEW_EDGE_1) {
                                        ui_action = UIA_NEW_EDGE_2;
                                } else if (ui_action == UIA_NEW_H_EDGE_1) {
                                        ui_action = UIA_NEW_H_EDGE_2;
                                } else if (ui_action == UIA_RENAME) {
                                        ui_action = UIA_ENTER_RENAME;
                                        input_len = 0;
                                        input_idx = 0;
                                        input[0] = '\0';
                                        SDL_StartTextInput();

                                        /* The nul terminator bleeds over into input */
                                        strncpy(input_with_prefix, "New name: ",
                                                11);
                                        rerender_input_string = 1;
                                }

                                break;
                        case UIA_NEW_EDGE_2:
                        case UIA_NEW_H_EDGE_2:

                                if (selected_vertex == (size_t) -1 ||
                                    selected_vertex == last_clicked_vertex) {
                                        switch (ui_action) {
                                        case UIA_NEW_EDGE_2:
                                                ui_action = UIA_NEW_EDGE_1;
                                                break;
                                        case UIA_NEW_H_EDGE_2:
                                                ui_action = UIA_NEW_H_EDGE_1;
                                                break;
                                        default:
                                                ui_action = UIA_NONE;
                                        }

                                        last_clicked_vertex = (size_t) -1;
                                        break;
                                }

                                ui_e = (struct ui_event) {
                                        /* */
                                        .type = ET_NEW_EDGE, .idx_1 =
                                                last_clicked_vertex, .idx_2 =
                                                selected_vertex, .a = 1, .b =
                                                (ui_action == UIA_NEW_EDGE_2 ?
                                                 1 : 2)
                                };
                                ret = eq_push(&ui_e);

                                if (!ui_action_loop) {
                                        ui_action = UIA_NONE;
                                } else if (ui_action == UIA_NEW_EDGE_2) {
                                        ui_action = UIA_NEW_EDGE_1;
                                } else {
                                        ui_action = UIA_NEW_H_EDGE_1;
                                }

                                last_clicked_vertex = (size_t) -1;
                                break;
                        case UIA_DELETE:

                                if (selected_vertex != (size_t) -1) {
                                        ui_e = (struct ui_event) {
                                                /* */
                                                .type = ET_DELETE_VERTEX,
                                                .idx_1 = selected_vertex
                                        };
                                        ret = eq_push(&ui_e);
                                } else if (selected_edge_i != (size_t) -1 &&
                                           selected_edge_j != (size_t) -1) {
                                        ui_e = (struct ui_event) {
                                                /* */
                                                .type = ET_DELETE_EDGE, .idx_1 =
                                                        selected_edge_i,
                                                .idx_2 =
                                                        selected_edge_j
                                        };
                                        ret = eq_push(&ui_e);
                                }

                                ui_action = ui_action_loop ? UIA_DELETE :
                                            UIA_NONE;
                                break;
                        case UIA_INC_FATNESS:
                        case UIA_DEC_FATNESS:

                                if (selected_vertex == (size_t) -1) {
                                        break;
                                }

                                ui_e = (struct ui_event) {
                                        /* */
                                        .type = ET_CHANGE_FATNESS, .idx_1 =
                                                selected_vertex, .int_1 =
                                                (ui_action ==
                                                 UIA_INC_FATNESS
                                                 ? 1 : -1)
                                };
                                ret = eq_push(&ui_e);

                                if (!ui_action_loop) {
                                        ui_action = UIA_NONE;
                                }

                                break;
                        case UIA_NONE:
                        case UIA_ASK_QUIT:
                        case UIA_ENTER_SAVE:
                        case UIA_ENTER_LOAD:
                        case UIA_ENTER_RENAME:
                        case UIA_ENTER_COLOR:
                        case UIA_LEN:
                                break;
                        }

                        break;
                }

                if (ret) {
                        goto done;
                }
        }

        if (load_requested ||
            save_requested) {
                save_load_delay = 30;
                char *f = 0;
                int r = 0;

                if (load_requested) {
                        r = choose_load_file(&f);
                } else {
                        r = choose_save_file(&f);
                }

                if (!r) {
                        if (f) {
                                ui_e = (struct ui_event) { .str = f };
                                ui_e.type = load_requested ? ET_LOAD : ET_SAVE;

                                /* f is freed on next ui_finish_frame */
                                free_this = f;
                                ret = eq_push(&ui_e);
                        }
                } else {
                        ui_action = load_requested ? UIA_ENTER_LOAD :
                                    UIA_ENTER_SAVE;
                        input_idx = 0;
                        input_len = 0;
                        input[0] = '\0';
                        SDL_StartTextInput();

                        /* The nul terminator bleeds over into input */
                        strncpy(input_with_prefix, "Filename: ", 11);
                        rerender_input_string = 1;
                }
        }

        /* framelimit */
        now = SDL_GetTicks();

        if (frame_start_ticks <= now) {
                Uint32 elapsed_time = now - frame_start_ticks;

                if (elapsed_time < TICKS_PER_FRAME) {
                        SDL_Delay(TICKS_PER_FRAME - elapsed_time);
                }
        }

done:

        return ret;
}

/* Return an event to the main loop */
int
ui_get_event(struct ui_event *e, uint_fast8_t *more)
{
        eq_pop(e);
        *more = eq_head != eq_tail;

        if (e) {
                switch (e->type) {
                case ET_NONE:
                case ET_LOAD:
                case ET_SAVE:
                case ET_QUIT:
                        break;
                case ET_CHANGE_FATNESS:
                case ET_CHANGE_COLOR:
                case ET_DELETE_EDGE:
                case ET_DELETE_VERTEX:
                case ET_MUTATE:
                case ET_NEW_EDGE:
                case ET_NEW_VERTEX:
                case ET_RENAME:
                        mark_as_edited();
                        break;
                }
        }

        return 0;
}
