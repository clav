.SUFFIXES:
.SUFFIXES: .o .c

CFLAGS ?=
LDFLAGS ?=

PKG_CONFIG ?= pkg-config

PCSTATIC ?=
ifneq (,$(findstring -static,$(LDFLAGS)))
	PCSTATIC += --static
endif

PREFIX ?= /usr
BINDIR ?= $(PREFIX)/bin
SHAREDIR ?= $(PREFIX)/share
MANDIR ?= $(PREFIX)/share/man

CFLAGS += -std=c99 -D_POSIX_C_SOURCE=200809L

# Debug
# CFLAGS += -fsanitize=address -g -O0 -pedantic -Wall -Wextra -Werror
# LDFLAGS += -fsanitize=address

# If you plan to use clav-mutation-find, it is highly recommended that you enable -O3
# CFLAGS += -O3 -Wall -Wextra -Werror

SDLPROG = clav-sdl
TEXTPROG = clav-cli
MUTFINDPROG = clav-mutation-find
BFZIIIPROG = clav-bfzIII
GRAPHISOPROG = clav-check-graph-isomorphism

default: all

.PHONY: all
all: $(SDLPROG) $(TEXTPROG) $(MUTFINDPROG) $(BFZIIIPROG) $(GRAPHISOPROG)

$(SDLPROG): clav.o quiver.o ui-sdl.o ui-sdl-font.o color-selection.o file-selection.o
	$(CC) -o $@ $^ $(LDFLAGS) -lm $(shell $(PKG_CONFIG) $(PCSTATIC) --libs sdl2 SDL2_ttf)

$(TEXTPROG): clav.o quiver.o ui-cli.o
	$(CC) -o $@ $^ $(LDFLAGS)

$(MUTFINDPROG): mutation-finder.o quiver.o
	$(CC) -o $@ $^ $(LDFLAGS)

$(BFZIIIPROG): bfzIII.o quiver.o
	$(CC) -o $@ $^ $(LDFLAGS)

$(GRAPHISOPROG): check-graph-iso.o quiver.o
	$(CC) -o $@ $^ $(LDFLAGS)

clav.o: clav.c macros.h ui.h quiver.h
	$(CC) $(CFLAGS) -c -o $@ $<

quiver.o: quiver.c macros.h ui.h quiver.h
	$(CC) $(CFLAGS) -c -o $@ $<

ui-sdl.o: ui-sdl.c macros.h ui.h quiver.h color-selection.h file-selection.h ui-sdl-font.h
	$(CC) $(CFLAGS) $(shell $(PKG_CONFIG) $(PCSTATIC) --cflags sdl2 SDL2_ttf) -c -o $@ $<

ui-sdl-font.o: ui-sdl-font.c
	$(CC) $(CFLAGS) -c -o $@ $<

ui-cli.o: ui-cli.c macros.h quiver.h
	$(CC) $(CFLAGS) -c -o $@ $<

file-selection.o: file-selection.c file-selection.h macros.h
	$(CC) $(CFLAGS) -c -o $@ $<

color-selection.o: color-selection.c color-selection.h macros.h
	$(CC) $(CFLAGS) -c -o $@ $<

mutation-finder.o: mutation-finder.c macros.h quiver.h
	$(CC) $(CFLAGS) -c -o $@ $<

bfzIII.o: bfzIII.c macros.h quiver.h
	$(CC) $(CFLAGS) -c -o $@ $<

check-graph-iso.o: check-graph-iso.c macros.h quiver.h
	$(CC) $(CFLAGS) -c -o $@ $<

.PHONY: clean
clean:
	rm -f *.o
	rm -f *~
	rm -f $(SDLPROG)
	rm -f $(TEXTPROG)
	rm -f $(MUTFINDPROG)
	rm -f $(BFZIIIPROG)
	rm -f $(GRAPHISOPROG)

.PHONY: install
install: all
	mkdir -p $(DESTDIR)$(BINDIR)
	cp -f $(SDLPROG) $(DESTDIR)$(BINDIR)/
	cp -f $(TEXTPROG) $(DESTDIR)$(BINDIR)/
	cp -f $(MUTFINDPROG) $(DESTDIR)$(BINDIR)/
	cp -f $(BFZIIIPROG) $(DESTDIR)$(BINDIR)/
	cp -f $(GRAPHISOPROG) $(DESTDIR)$(BINDIR)/
	mkdir -p $(DESTDIR)$(SHAREDIR)/clav
	cp -f data/*.txt $(DESTDIR)$(SHAREDIR)/clav/
	mkdir -p $(DESTDIR)$(MANDIR)/man1
	cp -f clav.1 $(DESTDIR)$(MANDIR)/man1/
	cp -f clav-cli.1 $(DESTDIR)$(MANDIR)/man1/
	cp -f clav-sdl.1 $(DESTDIR)$(MANDIR)/man1/
	cp -f clav-mutation-find.1 $(DESTDIR)$(MANDIR)/man1/

.PHONY: uninstall
uninstall:
	cd $(DESTDIR)$(BINDIR) && rm -f $(SDLPROG)
	cd $(DESTDIR)$(BINDIR) && rm -f $(TEXTPROG)
	cd $(DESTDIR)$(BINDIR) && rm -f $(MUTFINDPROG)
	cd $(DESTDIR)$(BINDIR) && rm -f $(BFZIIIPROG)
	cd $(DESTDIR)$(BINDIR) && rm -f $(GRAPHISOPROG)
	cd $(DESTDIR)$(SHAREDIR) && rm -rf clav
	cd $(DESTDIR)$(MANDIR)/man1 && rm -f clav.1 clav-mutation-find.1 clav-cli.1 clav-sdl.1
