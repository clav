/*
 * Copyright (c) 2016-2019, S. Gilles <sgilles@math.umd.edu>
 *
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
enum event_type {
        ET_NONE = 0,
        ET_CHANGE_FATNESS,
        ET_CHANGE_COLOR,
        ET_DELETE_EDGE,
        ET_DELETE_VERTEX,
        ET_LOAD,
        ET_MUTATE,
        ET_NEW_EDGE,
        ET_NEW_VERTEX,
        ET_QUIT,
        ET_RENAME,
        ET_SAVE,
};

struct ui_event {
        /* What event this is */
        enum event_type type;

        /* Event-specific data */
        size_t idx_1;
        size_t idx_2;
        int int_1;
        int int_2;
        uint32_t z;
        int_fast32_t a;
        uint_fast32_t b;
        char *str;
};

/* Initialize whatever UI is present */
int ui_init(struct quiver *q);

/* Tear down whatever UI was made */
int ui_teardown(void);

/* Deal with the fact that the quiver was changed */
int ui_respond_quiver_change(void);

/* Acknowledge a successful load */
int ui_respond_successful_load(const char *filename);

/* Acknowledge a successful save */
int ui_respond_successful_save(const char *filename);

/* Record that a frame has been started */
int ui_start_frame(void);

/* Draw a frame, sleep to framelimit */
int ui_finish_frame(void);

/* Get some kind of ui-agnostic event (like `quit' or `mutate' */
int ui_get_event(struct ui_event *e, uint_fast8_t *more);
