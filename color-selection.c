/*
 * Copyright (c) 2016-2021, S. Gilles <sgilles@sgilles.net>
 *
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "macros.h"

/*
 * I am fully aware of how ugly this file is, which is why it's specially
 * split out, so that it can be completely redone one day, when some
 * kind of better solution exists.
 */
static uint_fast8_t try_kdialog = 1;
static uint_fast8_t try_qarma = 1;
static uint_fast8_t try_yad = 1;
static uint_fast8_t try_zenity = 1;
static uint_fast8_t try_xdialog = 1;
static char *cmd_kdialog = "kdialog --getcolor 2>/dev/null";
static char *cmd_qarma = "qarma --color-selection 2>/dev/null";
static char *cmd_yad = "yad --color 2>/dev/null";
static char *cmd_zenity = "zenity --color-selection 2>/dev/null";
static char *cmd_xdialog = "Xdialog --stdout --colorsel '' 0 0 2>/dev/null";

/* Convert result of fgetc into [0,16), with special case for EOF */
static int
atoh(int c, uint_fast8_t *early_eof)
{
        if (c == EOF) {
                *early_eof = 1;
        } else if ('0' <= c &&
                   c <= '9') {
                return c - '0';
        } else if ('a' <= c &&
                   c <= 'f') {
                return c - 'a' + 10;
        } else if ('A' <= c &&
                   c <= 'F') {
                return c - 'A' + 10;
        }

        return 0;
}

/* Read in #rrggbb, returned by most of these programs */
static int
read_hex(FILE *f, uint32_t *out_argb, uint_fast8_t *out_affirmed)
{
        int ret = 0;
        int pret = 0;
        uint32_t argb = 0;
        uint_fast8_t early_eof = 0;
        int c = '#';
        int n = 0;

        /* ignore leading '#', if there are any */
        do {
                c = fgetc(f);
                n++;
        }        while (n < 128 &&
                        !('0' <= c &&
                          c <= '9') &&
                        !('a' <= c &&
                          c <= 'f') &&
                        !('A' <= c &&
                          c <= 'F'));

        /* r */
        argb |= atoh(c, &early_eof) << 20;
        argb |= atoh(fgetc(f), &early_eof) << 16;

        /* g */
        argb |= atoh(fgetc(f), &early_eof) << 12;
        argb |= atoh(fgetc(f), &early_eof) << 8;

        /* b */
        argb |= atoh(fgetc(f), &early_eof) << 4;
        argb |= atoh(fgetc(f), &early_eof) << 0;
        ret = 0;

        /* Sometimes, zenity returns #rrrrggggbbbb */
        if (early_eof) {
                goto done;
        }

        if (fgetc(f) == EOF) {
                goto done;
        }

        if (fgetc(f) == EOF) {
                goto done;
        }

        /* We are certainly in #rrrrggggbbbb */
        argb = (argb & 0xff0000) | ((argb & 0xff) << 8);
        argb |= atoh(fgetc(f), &early_eof) << 4;
        argb |= atoh(fgetc(f), &early_eof) << 0;
done:
        pret = pclose(f);

        if (!ret &&
            WIFEXITED(pret) &&
            WEXITSTATUS(pret) != 127) {
                *out_argb = argb;
                *out_affirmed = !early_eof;
        } else {
                ret = 1;
                *out_argb = 0;
                *out_affirmed = 0;
        }

        return ret;
}

/* Read in rrr ggg bbb, returned by xdialog */
static int
read_spaced(FILE *f, uint32_t *out_argb, uint_fast8_t *out_affirmed)
{
        int ret = 0;
        int pret = 0;
        int c = 0;
        int n = 0;
        uint_fast8_t r = 0;
        uint_fast8_t g = 0;
        uint_fast8_t b = 0;
        uint_fast8_t s = 0;

        /* r */
        s = 0;

        while (1) {
                n++;
                c = fgetc(f);

                if (c == EOF ||
                    n > 128) {
                        *out_affirmed = 0;
                        goto done;
                } else if (s &&
                           (c == ' ' ||
                            c == ',')) {
                        break;
                } else if ('0' <= c &&
                           c <= '9') {
                        r = 10 * r + (c - '0');
                        s = 1;
                }
        }

        /* g */
        s = 0;

        while (1) {
                n++;
                c = fgetc(f);

                if (c == EOF ||
                    n > 128) {
                        *out_affirmed = 0;
                        goto done;
                } else if (s &&
                           (c == ' ' ||
                            c == ',')) {
                        break;
                } else if ('0' <= c &&
                           c <= '9') {
                        g = 10 * g + (c - '0');
                        s = 1;
                }
        }

        /* b */
        s = 0;

        while (1) {
                n++;
                c = fgetc(f);

                if (c == EOF ||
                    n > 128) {
                        *out_affirmed = s;
                        break;
                } else if (s &&
                           (c == ' ' ||
                            c == ',' ||
                            c == ')')) {
                        *out_affirmed = 1;
                        break;
                } else if ('0' <= c &&
                           c <= '9') {
                        b = 10 * b + (c - '0');
                        s = 1;
                }
        }

        ret = 0;
done:
        pret = pclose(f);

        if (!ret &&
            WIFEXITED(pret) &&
            WEXITSTATUS(pret) != 127) {
                *out_argb = (r << 16) | (g << 8) | (b << 0);
        } else {
                ret = 1;
                *out_argb = 0;
        }

        return ret;
}

/* read in EITHER rgb(rrr,ggg,bbb) or #rrggbb */
static int
read_zenity(FILE *f, uint32_t *out_argb, uint_fast8_t *out_affirmed)
{
        int c = fgetc(f);

        if (c == 'r') {
                /* Probably zenity's rgb(rrr,ggg,bbb,aaa) thing */
                fgetc(f);
                fgetc(f);
                fgetc(f);

                return read_spaced(f, out_argb, out_affirmed);
        } else if (c == '#') {
                return read_hex(f, out_argb, out_affirmed);
        }

        /* This also covers the EOF case */
        *out_affirmed = 0;

        return 0;
}

int
choose_color(uint32_t *out_color, uint_fast8_t *out_affirmed)
{
        uint32_t argb = 0;
        uint_fast8_t affirmed = 0;
        int ret = ENOTSUP;
        FILE *f = 0;

        fflush(0);

        if (try_kdialog) {
                if ((f = popen(cmd_kdialog, "r"))) {
                        if (!(ret = read_hex(f, &argb, &affirmed))) {
                                goto done;
                        }
                }

                try_kdialog = 0;
        }

        if (try_qarma) {
                if ((f = popen(cmd_qarma, "r"))) {
                        if (!(ret = read_hex(f, &argb, &affirmed))) {
                                goto done;
                        }
                }

                try_qarma = 0;
        }

        if (try_yad) {
                if ((f = popen(cmd_yad, "r"))) {
                        if (!(ret = read_hex(f, &argb, &affirmed))) {
                                goto done;
                        }
                }

                try_yad = 0;
        }

        if (try_zenity) {
                if ((f = popen(cmd_zenity, "r"))) {
                        if (!(ret = read_zenity(f, &argb, &affirmed))) {
                                goto done;
                        }
                }

                try_zenity = 0;
        }

        if (try_xdialog) {
                if ((f = popen(cmd_xdialog, "r"))) {
                        if (!(ret = read_spaced(f, &argb, &affirmed))) {
                                goto done;
                        }
                }

                try_xdialog = 0;
        }

        ret = ENOTSUP;
done:

        if (!ret) {
                *out_color = argb;
                *out_affirmed = affirmed;
        }

        return ret;
}
