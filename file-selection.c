/*
 * Copyright (c) 2016-2021, S. Gilles <sgilles@sgilles.net>
 *
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "macros.h"

/*
 * I am fully aware of how ugly this file is, which is why it's specially
 * split out, so that it can be completely redone one day, when some
 * kind of better solution exists.
 */
static uint_fast8_t try_kdialog = 1;
static uint_fast8_t try_qarma = 1;
static uint_fast8_t try_yad = 1;
static uint_fast8_t try_zenity = 1;
static uint_fast8_t try_xdialog = 1;
enum action { ACT_SAVE, ACT_LOAD };
static char *cmd_kdialog[] = {
        /* */
        [ACT_SAVE] = "kdialog --getsavefilename :clav 2>/dev/null", /* */
        [ACT_LOAD] = "kdialog --getopenfilename :clav 2>/dev/null", /* */
};
static char *cmd_qarma[] = {
        /* */
        [ACT_SAVE] =
                "qarma --title 'Save As' --file-selection --save 2>/dev/null", /* */
        [ACT_LOAD] = "qarma --title 'Load' --file-selection 2>/dev/null",      /* */
};
static char *cmd_yad[] = {
        /* */
        [ACT_SAVE] =
                "yad --title 'Save As' --width=640 --height=480 --file-selection --save 2>/dev/null", /* */
        [ACT_LOAD] =
                "yad --title 'Load' --width=640 --height=480 --file-selection 2>/dev/null",           /* */
};
static char *cmd_zenity[] = {
        /* */
        [ACT_SAVE] =
                "zenity --title 'Save As' --file-selection --save 2>/dev/null", /* */
        [ACT_LOAD] = "zenity --title 'Load' --file-selection 2>/dev/null",      /* */
};
static char *cmd_xdialog[] = {
        /* */
        [ACT_SAVE] =
                "Xdialog --stdout --title 'Save As' --fselect '' 0 0 2>/dev/null",        /* */
        [ACT_LOAD] =
                "Xdialog --stdout --title 'Load' --fselect '' 0 0 2>/dev/null",           /* */
};

/* Naively slurp stream in completly, truncate last `\n'. */
static int
slurp(FILE *f, char **out)
{
        static size_t read_size = 1 << 7;
        int ret = 0;
        int pret = 0;
        size_t len = 0;
        size_t sz = read_size - 1;
        char *buf = 0;

        /* No need to check for overflow; sz + 1 == 1 << 7 */
        if (!(buf = malloc(sz + 1))) {
                ret = errno;
                perror(L("malloc"));
                goto done;
        }

        char *p = buf;

        *p = '\0';

        do {
                if (!fgets(buf + len, read_size, f)) {
                        break;
                }

                p = strchr(buf + len, '\0');
                len = p - buf;
                sz += read_size;
                void *newmem = 0;

                if (sz + 1 < sz) {
                        ret = errno = EOVERFLOW;
                        perror(L(""));
                        goto done;
                }

                if (!(newmem = realloc(buf, sz + 1))) {
                        ret = errno;
                        perror(L("realloc"));
                        goto done;
                }

                buf = newmem;
        } while (1);

        if (len) {
                if ((p = strrchr(buf + len - 1, '\n'))) {
                        *p = '\0';
                }
        }

        ret = 0;
done:
        pret = pclose(f);

        if (!ret &&
            WIFEXITED(pret) &&
            WEXITSTATUS(pret) != 127) {
                *out = buf;
        } else {
                ret = 1;
                free(buf);
                buf = 0;
                *out = 0;
        }

        return ret;
}

static int
choose_file(char **out_filename, enum action act)
{
        int ret = ENOTSUP;
        char *filename = 0;
        FILE *f = 0;

        fflush(0);

        if (try_kdialog) {
                if ((f = popen(cmd_kdialog[act], "r"))) {
                        if (!(ret = slurp(f, &filename))) {
                                goto done;
                        }
                }

                free(filename);
                filename = 0;
                try_kdialog = 0;
        }

        if (try_qarma) {
                if ((f = popen(cmd_qarma[act], "r"))) {
                        if (!(ret = slurp(f, &filename))) {
                                goto done;
                        }
                }

                free(filename);
                filename = 0;
                try_qarma = 0;
        }

        if (try_yad) {
                if ((f = popen(cmd_yad[act], "r"))) {
                        if (!(ret = slurp(f, &filename))) {
                                goto done;
                        }
                }

                free(filename);
                filename = 0;
                try_yad = 0;
        }

        if (try_zenity) {
                if ((f = popen(cmd_zenity[act], "r"))) {
                        if (!(ret = slurp(f, &filename))) {
                                goto done;
                        }
                }

                free(filename);
                filename = 0;
                try_zenity = 0;
        }

        if (try_xdialog) {
                if ((f = popen(cmd_xdialog[act], "r"))) {
                        if (!(ret = slurp(f, &filename))) {
                                goto done;
                        }
                }

                free(filename);
                filename = 0;
                try_xdialog = 0;
        }

        ret = ENOTSUP;
done:

        if (!ret) {
                *out_filename = filename;
        } else {
                free(filename);
        }

        return ret;
}

int
choose_save_file(char **out_filename)
{
        return choose_file(out_filename, ACT_SAVE);
}

int
choose_load_file(char **out_filename)
{
        return choose_file(out_filename, ACT_LOAD);
}
