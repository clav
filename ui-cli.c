/*
 * Copyright (c) 2016-2021, S. Gilles <sgilles@sgilles.net>
 *
 * Permission to use, copy, modify, and/or distribute this software
 * for any purpose with or without fee is hereby granted, provided
 * that the above copyright notice and this permission notice appear
 * in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "macros.h"
#include "quiver.h"
#include "ui.h"

/* Do special things on first frame */
static uint_fast8_t first_time = 1;

/* The quiver */
struct quiver *q;

/* Max line length to accept as command */
static size_t bufsize = 1 << 11;

/* Memory which must be passed back to clav.c, then freed */
static void *free_this = 0;

/* Intialize CLI */
int
ui_init(struct quiver *i_q)
{
        q = i_q;

        return 0;
}

/* Acknowledge a successful load */
int
ui_respond_successful_load(const char *filename)
{
        printf("Loaded %s\n", filename);

        return 0;
}

/* Acknowledge a successful save */
int
ui_respond_successful_save(const char *filename)
{
        printf("Saved to %s\n", filename);

        return 0;
}

/* Deal with the fact that the quiver was changed */
int
ui_respond_quiver_change(void)
{
        return 0;
}

/* Tear down CLI */
int
ui_teardown(void)
{
        if (free_this) {
                free(free_this);
                free_this = 0;
        }

        return 0;
}

/* Record that a frame has been started */
int
ui_start_frame(void)
{
        return 0;
}

/* Draw a frame, sleep to framelimit */
int
ui_finish_frame(void)
{
        return 0;
}

/* Print help */
static void
print_help(void)
{
        printf(
                "------------------------------------------------------------\n");
        printf("help                        This message\n");
        printf("print                       Display the quiver\n");
        printf("mutate <num>                Mutate at <num>\n");
        printf(
                "mutatename <str>            Mutate at vertex with name <str>\n");
        printf("delete <num>                Delete vertex <num>\n");
        printf(
                "delete <num1> <num2>        Delete edges between <num> and <num>\n");
        printf(
                "vertex <v>                  Create vertex with name <v>, fatness 1\n");
        printf(
                "edge <num1> <num2> <p>/<q>  Add an edge from <num> to <num>, weight <p>/<q>\n");
        printf("rename <num> <str>          Rename vertex <num> to <str>\n");
        printf("renamename <str1> <str2>    Rename vertex <str1> to <str2>\n");
        printf(
                "incfat <num>                Increase fatness of vertex <num> by 1\n");
        printf(
                "decfat <num>                Decrease fatness of vertex <num> by 1\n");
        printf("save <str>                  Write quiver to file <str>\n");
        printf("load <str>                  Load quiver from file <str>\n");
        printf("quit                        Quit\n");
        printf(
                "------------------------------------------------------------\n");
}

/* Print current quiver */
static void
print_quiver(void)
{
        size_t i = 0;
        size_t j = 0;
        struct rational *e = 0;
        size_t line_length = 0;

        printf("Vertices:\n");
        printf("%*s | Name\n-----------\n", 4, "i");

        for (j = 0; j < q->v_num; ++j) {
                printf("%*llu | %s\n", 4, (long long unsigned) j, q->v[j].name);
        }

        putchar('\n');
        printf("Edges (i -> j):\n");
        line_length = printf("%*s| ", 4, "i\\j");

        for (j = 0; j < q->v_num; ++j) {
                line_length += printf("%*llu", 5, (long long unsigned) j);
        }

        putchar('\n');

        for (j = 0; j < line_length; ++j) {
                putchar('-');
        }

        for (i = 0; i < q->v_num; ++i) {
                printf("\n %*llu| ", 3, (long long unsigned) i);

                for (j = 0; j < q->v_num; ++j) {
                        e = &(q->e[i * q->v_len + j]);

                        if (e->p == 0) {
                                printf("%*s", 5, "");
                        } else if (e->q == 1) {
                                printf("%*lld", 5, (long long int) e->p);
                        } else {
                                printf("%*lld/%llu", 3, (long long int) e->p,
                                       (long long unsigned) e->q);
                        }
                }
        }

        printf("\n");
        fflush(stdout);
}

/* Check for `help' */
static int
check_help(char *buf)
{
        return strcmp(buf, "help");
}

/* Check for `print' */
static int
check_print(char *buf)
{
        return strcmp(buf, "print");
}

/* Check for `quit' */
static int
check_quit(char *buf)
{
        return strcmp(buf, "quit");
}

/* Check for `mutate' */
static int
check_mutate(char *buf, struct ui_event *e, uint_fast8_t *partial_match)
{
        size_t i = 0;
        char dummy = 0;

        if (strncmp(buf, "mutate", 6)) {
                return 1;
        }

        *partial_match = 1;

        if (sscanf(buf, "mutate %zu%c", &i, &dummy) != 1) {
                printf("Type `help' to see how to use `mutate'\n");

                return 1;
        }

        if (i >= q->v_num) {
                printf("Cannot mutate at %zu: not in quiver\n", i);

                return 1;
        }

        *e = (struct ui_event) { .type = ET_MUTATE, .idx_1 = i };

        return 0;
}

/* Check for `mutatename' */
static int
check_mutatename(char *buf, struct ui_event *e, uint_fast8_t *partial_match)
{
        char *s = 0;
        size_t i = q->v_num;
        char dummy = 0;
        int ret = 1;

        if (!(s = malloc(1024))) {
                perror(L("malloc"));
                goto done;
        }

        if (strncmp(buf, "mutatename", 10)) {
                goto done;
        }

        *partial_match = 1;

        if (sscanf(buf, "mutatename %1023s %c", s, &dummy) != 1) {
                printf("Type `help' to see how to use `mutatename'\n");
                goto done;
        }

        for (size_t j = 0; j < q->v_num; ++j) {
                if (!(strcmp(q->v[j].name, s))) {
                        i = j;
                        break;
                }
        }

        if (i >= q->v_num) {
                printf("Cannot mutate at %s: not in quiver\n", s);
                goto done;
        }

        ret = 0;
        *e = (struct ui_event) { .type = ET_MUTATE, .idx_1 = i };
done:
        free(s);

        return ret;
}

/* Check for `delete' */
static int
check_delete(char *buf, struct ui_event *e, uint_fast8_t *partial_match)
{
        size_t i = 0;
        size_t j = 0;
        char dummy = 0;

        if (strncmp(buf, "delete", 6)) {
                return 1;
        }

        *partial_match = 1;
        int r = sscanf(buf, "delete %zu %zu%c", &i, &j, &dummy);

        if (r < 1 &&
            r > 2) {
                printf("Type `help' to see how to use `delete'\n");

                return 1;
        }

        if (r == 1) {
                if (i >= q->v_num) {
                        printf("Cannot delete %zu: not in quiver\n", i);

                        return 1;
                } else {
                        *e = (struct ui_event) { .type = ET_DELETE_VERTEX,
                                                 .idx_1 = i };
                }
        } else if (r == 2) {
                if (i >= q->v_num ||
                    j >= q->v_num) {
                        printf(
                                "Cannot delete edges between %zu and %zu: not in quiver\n",
                                i, j);

                        return 1;
                } else {
                        *e = (struct ui_event) { .type = ET_DELETE_EDGE,
                                                 .idx_1 = i, .idx_2 = j };
                }
        }

        return 0;
}

/* Check for `vertex' */
static int
check_vertex(char *buf, struct ui_event *e, uint_fast8_t *partial_match)
{
        char *s = 0;
        char dummy = 0;
        int ret = 0;

        if (!(s = malloc(1024))) {
                perror(L("malloc"));
                goto done_err;
        }

        if (strncmp(buf, "vertex", 6)) {
                goto done_err;
        }

        *partial_match = 1;

        if (sscanf(buf, "vertex %1023s %c", s, &dummy) != 1) {
                printf("Type `help' to see how to use `vertex'\n");
                goto done_err;
        }

        *e = (struct ui_event) { .type = ET_NEW_VERTEX, .z = 0x8282b2, .str =
                                         s };
        free_this = s;
done:

        return ret;
done_err:
        ret = 1;
        free(s);
        goto done;
}

/* Check for `edge' */
static int
check_edge(char *buf, struct ui_event *e, uint_fast8_t *partial_match)
{
        size_t i = 0;
        size_t j = 0;
        int a = 0;
        int b = 1;
        char slash = 0;
        char dummy = 0;

        if (strncmp(buf, "edge", 4)) {
                return 1;
        }

        *partial_match = 1;
        int r = sscanf(buf, "edge %zu %zu %d %c %d%c", &i, &j, &a, &slash, &b,
                       &dummy);

        if ((r != 3 &&
             r != 5) ||
            (r >= 4 &&
             slash != '/')) {
                printf("Type `help' to see how to use `edge'\n");

                return 1;
        }

        if (i >= q->v_num ||
            j >= q->v_num) {
                printf("Cannot add edge from %zu to %zu: not in quiver\n", i,
                       j);

                return 1;
        }

        if (!b) {
                printf("Cannot add edge of weight %d/%d: division by zero\n", a,
                       b);

                return 1;
        }

        if (b < 0) {
                a *= -1;
                b *= -1;
        }

        if (a <= INT_FAST8_MIN ||
            a >= INT_FAST8_MAX ||
            b >= UINT_FAST8_MAX) {
                printf(
                        "Cannot add edge of weight %d/%d: representation out of range\n",
                        a, b);

                return 1;
        }

        *e = (struct ui_event) { .type = ET_NEW_EDGE, .idx_1 = i, .idx_2 = j,
                                 .a = a, .b = b };

        return 0;
}

/* Check for `renamename' */
static int
check_renamename(char *buf, struct ui_event *e, uint_fast8_t *partial_match)
{
        size_t i = 0;
        size_t j = 0;
        uint_fast8_t seen = 0;
        char *s = 0;
        char *t = 0;
        char dummy = 0;
        int ret = 0;

        if (!(s = malloc(1024))) {
                perror(L("malloc"));
                goto done_err;
        }

        if (!(t = malloc(1024))) {
                perror(L("malloc"));
                goto done_err;
        }

        if (strncmp(buf, "renamename", 10)) {
                goto done_err;
        }

        *partial_match = 1;

        if (sscanf(buf, "renamename %1023s %1023s %c", s, t, &dummy) != 2) {
                printf("Type `help' to see how to use `renamename'\n");
                goto done_err;
        }

        for (j = 0; j < q->v_num; ++j) {
                if (!(strcmp(q->v[j].name, s))) {
                        i = j;
                        seen++;
                }
        }

        if (seen < 1) {
                printf("Cannot rename %s: not in quiver\n", s);
                goto done_err;
        }

        if (seen > 1) {
                printf("Cannot rename %s: ambiguous\n", s);
                goto done_err;
        }

        ret = 0;
        *e = (struct ui_event) { .type = ET_RENAME, .idx_1 = i, .str = t };
        free(s);
        free_this = t;
done:

        return ret;
done_err:
        ret = 1;
        free(s);
        free(t);
        goto done;
}

/* Check for `rename' */
static int
check_rename(char *buf, struct ui_event *e, uint_fast8_t *partial_match)
{
        size_t i = 0;
        char *s = 0;
        char dummy = 0;
        int ret = 0;

        if (!(s = malloc(1024))) {
                perror(L("malloc"));
                goto done_err;
        }

        if (strncmp(buf, "rename", 6)) {
                goto done_err;
        }

        *partial_match = 1;

        if (sscanf(buf, "rename %zu %1023s %c", &i, s, &dummy) != 2) {
                printf("Type `help' to see how to use `rename'\n");
                goto done_err;
        }

        if (i >= q->v_num) {
                printf("Cannot rename %zu: not in quiver\n", i);
                goto done_err;
        }

        ret = 0;
        *e = (struct ui_event) { .type = ET_RENAME, .idx_1 = i, .str = s };
        free_this = s;
done:

        return ret;
done_err:
        ret = 1;
        free(s);
        goto done;
}

/* Check for `incfat' */
static int
check_incfat(char *buf, struct ui_event *e, uint_fast8_t *partial_match)
{
        size_t i = 0;
        char dummy = 0;

        if (strncmp(buf, "incfat", 6)) {
                return 1;
        }

        *partial_match = 1;

        if (sscanf(buf, "incfat %zu%c", &i, &dummy) != 1) {
                printf("Type `help' to see how to use `incfat'\n");

                return 1;
        }

        if (i >= q->v_num) {
                printf("Cannot increase fatness of %zu: not in quiver\n", i);

                return 1;
        }

        if (q->v[i].fatness >= UINT_FAST8_MAX) {
                printf("Cannot increase fatness of %zu: unrepresentable\n", i);

                return 1;
        }

        *e = (struct ui_event) { .type = ET_CHANGE_FATNESS, .idx_1 = i, .int_1 =
                                         1 };

        return 0;
}

/* Check for `decfat' */
static int
check_decfat(char *buf, struct ui_event *e, uint_fast8_t *partial_match)
{
        size_t i = 0;
        char dummy = 0;

        if (strncmp(buf, "decfat", 6)) {
                return 1;
        }

        *partial_match = 1;

        if (sscanf(buf, "decfat %zu%c", &i, &dummy) != 1) {
                printf("Type `help' to see how to use `decfat'\n");

                return 1;
        }

        if (i >= q->v_num) {
                printf("Cannot decrease fatness of %zu: not in quiver\n", i);

                return 1;
        }

        if (q->v[i].fatness <= 1) {
                printf(
                        "Cannot decrease fatness of %zu: fatness must be positive\n",
                        i);

                return 1;
        }

        *e = (struct ui_event) { .type = ET_CHANGE_FATNESS, .idx_1 = i, .int_1 =
                                         -1 };

        return 0;
}

/* Check for `save' */
static int
check_save(char *buf, struct ui_event *e, uint_fast8_t *partial_match)
{
        char *s = 0;
        char dummy = 0;
        int ret = 0;

        if (!(s = malloc(1024))) {
                perror(L("malloc"));
                goto done_err;
        }

        if (strncmp(buf, "save", 4)) {
                goto done_err;
        }

        *partial_match = 1;

        if (sscanf(buf, "save %1023s %c", s, &dummy) != 1) {
                printf("Type `help' to see how to use `save'\n");
                goto done_err;
        }

        if (!s) {
                printf("Cannot save: some kind of resource allocation error\n");
                goto done_err;
        }

        ret = 0;
        *e = (struct ui_event) { .type = ET_SAVE, .str = s };
        free_this = s;
done:

        return ret;
done_err:
        ret = 1;
        free(s);
        goto done;
}

/* Check for `load' */
static int
check_load(char *buf, struct ui_event *e, uint_fast8_t *partial_match)
{
        char *s = 0;
        char dummy = 0;

        if (!(s = malloc(1024))) {
                perror(L("malloc"));

                return 1;
        }

        free_this = s;

        if (strncmp(buf, "load", 4)) {
                return 1;
        }

        *partial_match = 1;

        if (sscanf(buf, "load %1023s %c", s, &dummy) != 1) {
                printf("Type `help' to see how to use `load'\n");

                return 1;
        }

        if (!s) {
                printf("Cannot load: some kind of resource allocation error\n");

                return 1;
        }

        *e = (struct ui_event) { .type = ET_LOAD, .str = s };

        return 0;
}

/* Where the meat of this UI is.  Read commands until events are needed */
int
ui_get_event(struct ui_event *e, uint_fast8_t *more)
{
        char buf[bufsize];
        uint_fast8_t partial_match = 0;

        if (first_time) {
                first_time = 0;
                print_quiver();
                print_help();
        }

        printf("> ");
        fflush(stdout);

        while (fgets(buf, bufsize, stdin)) {
                buf[strcspn(buf, "\n")] = '\0';
                partial_match = 0;

                if (free_this) {
                        free(free_this);
                        free_this = 0;
                }

                if (!buf[0]) {
                        continue;
                }

                if (!check_help(buf)) {
                        print_help();
                } else if (!check_print(buf)) {
                        print_quiver();
                } else if (!check_quit(buf)) {
                        break;
                } else if (!partial_match &&
                           !check_mutatename(buf, e, &partial_match)) {
                        goto have_something;
                } else if (!partial_match &&
                           !check_mutate(buf, e, &partial_match)) {
                        goto have_something;
                } else if (!partial_match &&
                           !check_delete(buf, e, &partial_match)) {
                        goto have_something;
                } else if (!partial_match &&
                           !check_vertex(buf, e, &partial_match)) {
                        goto have_something;
                } else if (!partial_match &&
                           !check_edge(buf, e, &partial_match)) {
                        goto have_something;
                } else if (!partial_match &&
                           !check_renamename(buf, e, &partial_match)) {
                        goto have_something;
                } else if (!partial_match &&
                           !check_rename(buf, e, &partial_match)) {
                        goto have_something;
                } else if (!partial_match &&
                           !check_incfat(buf, e, &partial_match)) {
                        goto have_something;
                } else if (!partial_match &&
                           !check_decfat(buf, e, &partial_match)) {
                        goto have_something;
                } else if (!partial_match &&
                           !check_save(buf, e, &partial_match)) {
                        goto have_something;
                } else if (!partial_match &&
                           !check_load(buf, e, &partial_match)) {
                        goto have_something;
                } else if (!partial_match) {
                        printf("Unknown command (try `help')\n");
                }

                printf("> ");
                fflush(stdout);
        }

        /* ^D works like `quit' */
        printf("\n");
        *e = (struct ui_event) { .type = ET_QUIT };
have_something:
        *more = 0;

        return 0;
}
